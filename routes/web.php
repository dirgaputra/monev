<?php

use Illuminate\Support\Facades\Route;


Auth::routes();

Route::group(['middleware'=>['auth']], function() {
    Route::get('/', function () { 
        return redirect('/dashboard');
    });
    
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/user', 'UserController@index')->name('user');
    
    // account
    Route::get('/account/all', 'SosmedController@account')->name('account');
    Route::get('/account/create', 'SosmedController@createAccount')->name('create_account');
    Route::post('/account/create', 'SosmedController@submitAccount')->name('submit_account');

    Route::get('/account/edit/{id}', 'SosmedController@editSosmed')->name('edit');
    Route::post('/account/edit/{id}', 'SosmedController@updateAccount')->name('update_account');

    Route::get('/account/remove', 'SosmedController@removeSosmed')->name('remove_sosmed');
    Route::get('/account/remove/account', 'SosmedController@removeAccount')->name('remove_account');

    Route::get('/account/instagram', 'SosmedController@instagram')->name('instagram');
    Route::get('/account/youtube', 'SosmedController@youtube')->name('youtube');
    Route::get('/account/facebook', 'SosmedController@facebook')->name('facebook');
    Route::get('/auth', 'SosmedController@handleProviderCallback');
    Route::get('/account/twitter', 'SosmedController@twitter')->name('twitter');

    Route::get('/getIG', 'FetchSosmedController@getIG')->name('getIG');
    Route::get('/getYT', 'FetchSosmedController@getYT')->name('getYT');
    Route::get('/getTWT', 'FetchSosmedController@getTWT')->name('getTWT');

    Route::get('/getPolda', 'SubRoleController@getPolda')->name('getPolda');
    Route::get('/getPolres', 'SubRoleController@getPolres')->name('getPolres');

    // activity
    Route::get('/activity/cat/all', 'ActivityController@all')->name('activityAll');
    Route::get('/activity/cat/{slug}', 'ActivityController@index')->name('activity');
    Route::get('/activity/detail/{id}', 'ActivityController@detail')->name('detailActivity');
    Route::get('/activity/cat/{slug}/export', 'ActivityController@export')->name('export');
    Route::get('/activity/create', 'ActivityController@create')->name('create_activity');
    Route::post('/activity/create', 'ActivityController@store');
    Route::get('/activity/edit/{id}', 'ActivityController@edit')->name('edit_activity');
    Route::post('/activity/edit/{id}', 'ActivityController@editStore');
    Route::get('/activity/delete', 'ActivityController@delete');

    
    // middleware
    Route::group(['middleware'=>['is_admin']], function() {
        // user
        Route::get('/user/create', 'UserController@create')->name('create_user');
        Route::post('/user/create', 'UserController@submit')->name('submit_user');
        Route::get('/user/edit/{id}', 'UserController@edit')->name('edit_user');
        Route::post('/user/edit/{id}', 'UserController@storeEdit');
        Route::get('/user/password/{id}', 'UserController@password')->name('edit_password');
        Route::post('/user/password/{id}', 'UserController@storePassword');
        Route::get('/user/delete', 'UserController@delete');

        //video
        Route::get('/video', 'VideoController@index')->name('videoAll');
        Route::get('/video/create', 'VideoController@create')->name('create_video');
        Route::post('/video/create', 'VideoController@store');
        Route::get('/video/edit/{id}', 'VideoController@edit');
        Route::post('/video/edit/{id}', 'VideoController@editStore');
        Route::get('/video/delete', 'VideoController@delete');

        // slider
        Route::get('/slider', 'SliderController@index')->name('sliderAll');
        Route::get('/slider/create', 'SliderController@create')->name('create_slider');
        Route::post('/slider/create', 'SliderController@store');
        Route::get('/slider/edit/{id}', 'SliderController@edit')->name('edit_slider');
        Route::post('/slider/edit/{id}', 'SliderController@editStore');
        Route::get('/slider/delete', 'SliderController@delete');
    });
});
