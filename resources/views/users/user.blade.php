@extends('layouts.app')
@section('content')
@include('layouts.headerUser')
  
  {{-- <div class="nk-block-head nk-block-head-sm">
    <div class="nk-block-between">
      <div class="nk-block-head-content">
        <h3 class="nk-block-title page-title">{{ $title }} ({{$total}})</h3>
      </div>
      <div class="nk-block-head-content">
        <div class="toggle-wrap nk-block-tools-toggle">
          <a href="{{ route('create_user') }}"><button class="btn btn-primary">Tambah Akun User</button></a>
        </div>
      </div>
    </div>
  </div> --}}

  <div class="nk-block">
    <div class="nk-tb-list is-separate mb-3">
      <div class="nk-tb-item nk-tb-head">
        <div class="nk-tb-col"><span class="sub-text">Nama</span></div>
        <div class="nk-tb-col"><span class="sub-text">Email</span></div>
        <div class="nk-tb-col"><span class="sub-text">Role</span></div>
        <div class="nk-tb-col"><span class="sub-text">Polri</span></div>
        <div class="nk-tb-col"><span class="sub-text">Polda</span></div>
        <div class="nk-tb-col"><span class="sub-text">Polres</span></div>
        <div class="nk-tb-col">&nbsp;</div>
      </div>
      @foreach ($users as $_users)
        <div class="nk-tb-item _list_{{ $_users->id }}">
          <div class="nk-tb-col">
            <span class="tb-amount">{{$_users->name}}</span>
          </div>
          <div class="nk-tb-col">
            <span>{{$_users->email}}</span>
          </div>
          <div class="nk-tb-col">
            <span>{{$_users->role->role_name}}</span>
          </div>
          <div class="nk-tb-col">
            <span>{{$_users->polri ? $_users->polri->name_polri : '-'}}</span>
          </div>
          <div class="nk-tb-col">
            <span>{{$_users->polda ? $_users->polda->name_polda : '-'}}</span>
          </div>
          <div class="nk-tb-col">
            <span>{{$_users->polres ? $_users->polres->name_polres : '-'}}</span>
          </div>

          <div class="nk-tb-col nk-tb-col-tools">
            @if(Auth::user()->role_id === 1)
              <div class="dropdown">
                <a class="text-soft dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown" data-offset="-8,0"><em class="icon ni ni-more-h"></em></a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-s">
                  <ul class="link-list-plain">
                    <li><a href="{{ url('/user/edit/'.$_users->uuid) }}" class="text-primary">Ubah Akun</a></li>
                    <li><a href="{{ url('/user/password/'.$_users->uuid) }}" class="text-primary">Ubah Password</a></li>
                    <li><a href="javascript:;" attr="{{ $_users->id }}" class="remove_sosmed text-danger">Hapus Akun</a></li>
                  </ul>
                </div>
              </div>
            @endif
          </div>
          
        </div>
      @endforeach
    </div>
    <div class="card">
      <div class="card-inner">
        {{ $users->links() }}
      </div>
    </div>
  </div>

@endsection


@section('script')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
    $(document).ready( function () {
      $('.remove_sosmed').on("click", function (e) {      
        var attr = $(this).attr('attr');
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
          if (result.value) {
            $.ajax({
              method: 'GET',
              url: `${window.location.origin}/user/delete`,
              dataType: 'json',
              data: { 'id': attr },
              success: function(result){
                $('._list_'+attr).remove();
                return Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
              }
            });
          }
        });
      });
    });
  </script>
@endsection
