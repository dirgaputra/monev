@extends('layouts.app')
@section('content')
  @if (session('error'))
    <div class="alert alert-danger" role="alert">{{ session('error') }}</div>
  @endif
 
  <div class="nk-block-head nk-block-head-sm">
    <div class="nk-block-between">
      <div class="nk-block-head-content">
        <h3 class="nk-block-title page-title">{{ $title }}</h3>
      </div>
    </div>
  </div>
  <div class="card card-preview">
    <div class="card-inner">
      <div class="preview-block">
        
        <form action="" method="post" class="form-validate">
          @csrf
          <div class="row gy-4">
            
            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label" for="default-01">Nama</label>
                <div class="form-control-wrap">
                  <input type="text" class="form-control" placeholder="Nama Anda" name="name" required autofocus>
                </div>
              </div>
            </div>
            
            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label" for="default-01">Email</label>
                <div class="form-control-wrap">
                  <input type="email" class="form-control" placeholder="example@me.com" name="email" required>
                </div>
              </div>
            </div>
            
            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label" for="default-01">Password</label>
                <div class="form-control-wrap">
                  <input type="password" class="form-control" placeholder="Password Anda" name="password" required>
                </div>
              </div>
            </div>
            
            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label" for="default-01">Confirm Password</label>
                <div class="form-control-wrap">
                  <input type="password" class="form-control" placeholder="Confirm Password Anda" name="cPassword" required>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label">Role</label>
                <div class="form-control-wrap">
                  <select class="form-select role yyyyy" name="role" required data-placeholder="Pilih Role">
                    <option label="empty" value=""></option>
                    @foreach ($role as $_role)
                      <option value="{{$_role->id}}">{{$_role->role_name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label">Polri</label>
                <div class="form-control-wrap">
                  <select class="form-select polri getPolda" name="polri" required disabled data-placeholder="Pilih Polri" data-search="on">
                    <option label="empty" value=""></option>
                    @foreach ($polri as $_polri)
                      <option value="{{$_polri->id}}">{{$_polri->name_polri}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
            
            <div class="col-sm-6 row_polda" style="display: none">
              <div class="form-group">
                <label class="form-label">Polda</label>
                <div class="form-control-wrap">
                  <select class="form-select polda getPolres" name="polda" disabled data-placeholder="Pilih Polda" data-search="on">
                    <option label="empty" value=""></option>
                  </select>
                </div>
              </div>
            </div>

            <div class="col-sm-6 row_polres" style="display: none">
              <div class="form-group">
                <label class="form-label">Polres</label>
                <div class="form-control-wrap">
                  <select class="form-select polres" name="polres" disabled  data-placeholder="Pilih Polres" data-search="on">
                    <option label="empty" value=""></option>
                  </select>
                </div>
              </div>
            </div>

            <div class="col-md-12 mt-5">
              <div class="form-group">
                <button type="submit" class="btn btn-lg btn-primary mr-3">Kirim</button>
                <a href="{{url('/user')}}" class="btn btn-lg btn-danger">Cancel</a>
              </div>
            </div>

          </div>

        </form>
      </div>
    </div>
  </div>
@endsection


@section('script')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
    $(document).ready( function () {
      var role;
      $('.role').on('change.select2', function (e) {
        $('.polri').removeAttr('disabled');
        var val = $(this).val();
        role = val 
        switch (val) {
          case '2':
            $('.polda, .polres').empty();
            return $('.row_polda, .row_polres').hide();
          case '3':
            $('.polres').empty();
            $('.row_polda').show()
            return $('.row_polres').hide();
          case '4':
            $('.row_polda').show()
            return $('.row_polres').show();
          default:
            $('.polda, .polres').empty();
            return $('.row_polda, .row_polres').hide();
        }
      });

      $('.getPolda').on('change', function () {
        if(role === "3" || role === "4"){
          var id = $(this).val();
          return getData('polda', id);
        }
      }).on('select2:closing', function (event) {
        if(role === "3" || role === "4"){
          var find = $(this).find(':selected');
          var id = find[0].attributes[0].value;
          return getData('polda', id);
        }
      });

      $('.getPolres').on('change', function () {
        if(role === "4"){
          var id = $(this).val();
          return getData('polres', id);
        }
      }).on('select2:closing', function (event) {
        if(role === "4"){
          var find = $(this).find(':selected');
          var id = find[0].attributes[0].value;
          return getData('polres', id);
        }
      });

      function getData(type, id){
        var path = type === 'polda' ? 'getPolda' : 'getPolres';
        $.ajax({
          method: 'GET',
          url: `${window.location.origin}/${path}`,
          dataType: 'json',
          data: { id: id },
          success: function(result){
            $('.'+type).empty();
            $.each(result.data, function (i, res) {
              var name = type === 'polda' ? res.name_polda : res.name_polres;
              $('.'+type).removeAttr('disabled')
              $('.'+type).append(new Option(name, res.id))
              $('.'+type).attr('required', '')
            })
          }
        });
      }

    });
  </script>
@endsection
