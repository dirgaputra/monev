@extends('layouts.app')
@section('content')
  @if (session('status'))
    <div class="alert alert-success" role="alert">{{ session('status') }}</div>
  @endif
  @if (session('error'))
    <div class="alert alert-danger" role="alert">{{ session('error') }}</div>
  @endif
  <div class="nk-block-head nk-block-head-sm">
    <div class="nk-block-between">
      <div class="nk-block-head-content">
        <h3 class="nk-block-title page-title">{{ $title }}</h3>
      </div>
    </div>
  </div>

  <div class="card card-preview">
    <div class="card-inner">
      <div class="preview-block">
        
        <form action="" method="post" class="form-validate">
          @csrf
          <div class="row gy-4">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label" for="default-01">Nama</label>
                <div class="form-control-wrap">
                  <input type="text" class="form-control" placeholder="Nama Anda" value="{{$users->name}}" name="name" required>
                </div>
              </div>
            </div>
            
            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label" for="default-01">Email</label>
                <div class="form-control-wrap">
                  <input type="text" class="form-control" placeholder="example@me.com" value="{{$users->email}}" name="email" required>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label">Role</label>
                <div class="form-control-wrap">
                  <select class="form-select role" name="role_id" required data-placeholder="Pilih Role">
                    @foreach ($role as $_role)
                      <option @if ($_role->id === $users->role->id ) selected @endif value="{{$_role->id}}">
                        {{$_role->role_name}}
                      </option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label">Polri</label>
                <div class="form-control-wrap">
                  <select class="form-select getPolda" name="polri" required data-placeholder="Pilih Polri"  data-search="on">
                    <option label="empty" value=""></option>
                    @foreach ($polri as $_polri)
                      <option value="{{$_polri->id}}" @if ($_polri->id === $users->polri->id ) selected @endif>{{$_polri->name_polri}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>

            
            <div class="col-sm-6 row_polda" style="display: {{$users->polda ? 'block' : 'none'}}">
              <div class="form-group">
                <label class="form-label">Polda</label>
                <div class="form-control-wrap">
                  <select class="form-select polda getPolres" name="polda" @if (!$users->polda) disabled @endif data-placeholder="Pilih Polda"  data-search="on">
                    <option label="empty" value=""></option>
                    @foreach ($polda as $_polda)
                      <option value="{{$_polda->id}}" @if ($_polda->id === $users->polda->id ) selected @endif>{{$_polda->name_polda}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>

            <div class="col-sm-6 row_polres" style="display: {{$users->polres ? 'block' : 'none'}}">
              <div class="form-group">
                <label class="form-label">Polres</label>
                <div class="form-control-wrap">
                  <select class="form-select polres" name="polres" @if (!$users->polres) disabled @endif  data-placeholder="Pilih Polres"  data-search="on">
                    <option label="empty" value=""></option>
                    @if($users->polres)
                      @foreach ($polres as $_polres)
                        <option value="{{$_polres->id}}" @if ($_polres->id === $users->polres->id ) selected @endif>{{$_polres->name_polres}}</option>
                      @endforeach
                    @endif
                  </select>
                </div>
              </div>
            </div>
            
            <div class="col-md-12 mt-5">
              <div class="form-group">
                <input type="text" value="{{$users->id}}" class="form-control" hidden name="id">
                <button type="submit" class="btn btn-lg btn-primary mr-3">Ubah</button>
                <a href="{{url('/user')}}" class="btn btn-lg btn-danger">Cancel</a>
              </div>
            </div>

          </div>

        </form>
      </div>
    </div>
  </div>

@endsection



@section('script')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
    $(document).ready( function () {
      var role = "{{$users->role->id}}";
      $('.role').on('change', function () {
        var val = $(this).val();
        switch (val) {
          case '2':
            $('.polda, .polres').empty();
            return $('.row_polda, .row_polres').hide();
          case '3':
            $('.polres').empty();
            $('.row_polda').show()
            return $('.row_polres').hide();
          case '4':
            $('.row_polda').show()
            return $('.row_polres').attr('aaaa', 'block');
          default:
            $('.polda, .polres').empty();
            return $('.row_polda, .row_polres').hide();
        }
      });

      $('.getPolda').on('change', function () {
        if(role === "3" || role === "4"){
          var id = $(this).val();
          return getData('polda', id);
        }
      }).on('select2:closing', function (event) {
        if(role === "3" || role === "4"){
          var find = $(this).find(':selected');
          var id = find[0].attributes[0].value;
          return getData('polda', id);
        }
      });

      $('.getPolres').on('change', function () {
        if(role === "4"){
          var id = $(this).val();
          return getData('polres', id);
        }
      }).on('select2:closing', function (event) {
        if(role === "4"){
          var find = $(this).find(':selected');
          var id = find[0].attributes[0].value;
          return getData('polres', id);
        }
      });

      function getData(type, id, cb){
        var path = type === 'polda' ? 'getPolda' : 'getPolres';
        $.ajax({
          method: 'GET',
          url: `${window.location.origin}/${path}`,
          dataType: 'json',
          data: { id: id },
          success: function(result){
            $('.'+type).empty()
            $.each(result.data, function (i, res) {
              var name = type === 'polda' ? res.name_polda : res.name_polres;
              $('.'+type).removeAttr('disabled')
              $('.'+type).append(new Option(name, res.id))
              $('.'+type).attr('required', '')
            })
          }
        });
      }

    });
  </script>
@endsection
