@extends('layouts.app')
@section('content')
  @if (session('status'))
    <div class="alert alert-success" role="alert">{{ session('status') }}</div>
  @endif
  @if (session('error'))
    <div class="alert alert-danger" role="alert">{{ session('error') }}</div>
  @endif
  <div class="nk-block-head nk-block-head-sm">
    <div class="nk-block-between">
      <div class="nk-block-head-content">
        <h3 class="nk-block-title page-title">{{ $title }}</h3>
      </div>
    </div>
  </div>

  <div class="card card-preview">
    <div class="card-inner">
      <div class="preview-block">
        
        <form action="" method="post" class="form-validate">
          @csrf
          <div class="row gy-4">
            <div class="col-sm-12">
              <div class="form-group">
                <label class="form-label" for="default-01">Nama</label>
                <div class="form-control-wrap">
                  <input type="text" class="form-control" disabled placeholder="Nama Anda" value="{{$users->name}}" name="name" required>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label" for="default-01">Password</label>
                <div class="form-control-wrap">
                  <input type="password" class="form-control" placeholder="Password Anda" name="password" required>
                </div>
              </div>
            </div>
            
            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label" for="default-01">Confirm Password</label>
                <div class="form-control-wrap">
                  <input type="password" class="form-control" placeholder="Confirm Password Anda" name="cPassword" required>
                </div>
              </div>
            </div>

            <div class="col-md-12 mt-5">
              <div class="form-group">
                <button type="submit" class="btn btn-lg btn-primary mr-3">Ubah</button>
                <a href="{{url('/user')}}" class="btn btn-lg btn-danger">Cancel</a>
              </div>
            </div>

          </div>

        </form>
      </div>
    </div>
  </div>

@endsection
