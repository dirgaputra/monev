<div class="nk-block">
  <div class="card card-full">
    <div class="card-header"><b>Total Followers Sosial Media</b></div>
    <div class="card-inner">
      <div class="chartWrapper">
        <div class="chartAreaWrapper">
          <canvas id="followers" style="height: 3500px; width: 100%;"></canvas>
        </div>
        <canvas id="myChartAxisPost"></canvas>
      </div>
    </div>
  </div>
</div>