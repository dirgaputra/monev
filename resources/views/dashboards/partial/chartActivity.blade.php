<div class="nk-block">
  <div class="card card-full">
    <div class="card-header"><b>Total Kegiatan Dikmas</b></div>
    <div class="card-inner">
      <div class="chartWrapper float-left w-100">
        <div class="chartAreaWrapper">
          <canvas id="activityChart" style="height: 1500px; width: 100%;"></canvas>
        </div>
        <canvas id="myChartAxisAct"></canvas>
      </div>

    </div>
  </div>
</div>

