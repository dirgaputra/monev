@extends('layouts.app')
@section('content')

  <div class="nk-block-head nk-block-head-sm">
    <div class="nk-block-between">
      <div class="nk-block-head-content">
        <h3 class="nk-block-title page-title">{{ $title }}</h3>
      </div>
    </div>
  </div>

  {{-- <div class="nk-block">
    <div class="row g-gs">
      <div class="col-sm-12">
        <div class="card p-2">
          <video controls="true" width="100%" height="315" class="embed-responsive-item" style="background: #000">
            <source src="{{$video->path}}" type="video/mp4" />
          </video>
        </div>
      </div>
    </div>
  </div> --}}
  <div class="col-6 offset-3 mb-5">
    <div id="carouselExConInd" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        @foreach ($slider as $index => $_slider)    
          <div class="carousel-item @if($index === 0) active @endif">
            <img src="{{$_slider->path}}" class="d-block w-100">
          </div>
        @endforeach
      </div>
      <a class="carousel-control-prev" href="#carouselExConInd" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExConInd" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
 
  @include('dashboards.partial.chartActivity')

  <div class="nk-block">
    <div class="row g-gs">
      <div class="col-sm-12">
        <h4 class="nk-block-title page-title text-center mt-5">Total Posts Sosial Media</h4>
        {{-- <h4 class="nk-block-title page-title text-center">Total Posts Sosial Media</h4> --}}
      </div>
      <div class="col-xxl-3 col-sm-3">
        <div class="card">
          <div class="nk-ecwg nk-ecwg6">
            <div class="card-inner">
              <div class="card-title-group">
                <div class="card-title">
                  <h6 class="title">Total Instagram</h6>
                </div>
              </div>
              <div class="data">
                <div class="data-group">
                  <div class="amount"><em class="icon ni ni-users"></em> {{ number_format($countIG) }}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xxl-3 col-sm-3">
        <div class="card">
          <div class="nk-ecwg nk-ecwg6">
            <div class="card-inner">
              <div class="card-title-group">
                <div class="card-title">
                  <h6 class="title">Total Facebook</h6>
                </div>
              </div>
              <div class="data">
                <div class="data-group">
                  <div class="amount"><em class="icon ni ni-users"></em> {{ number_format($countFB) }}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xxl-3 col-sm-3">
        <div class="card">
          <div class="nk-ecwg nk-ecwg6">
            <div class="card-inner">
              <div class="card-title-group">
                <div class="card-title">
                  <h6 class="title">Total Twitter</h6>
                </div>
              </div>
              <div class="data">
                <div class="data-group">
                  <div class="amount"><em class="icon ni ni-users"></em> {{ number_format($countTWT) }}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xxl-3 col-sm-3">
        <div class="card">
          <div class="nk-ecwg nk-ecwg6">
            <div class="card-inner">
              <div class="card-title-group">
                <div class="card-title">
                  <h6 class="title">Total Youtube</h6>
                </div>
              </div>
              <div class="data">
                <div class="data-group">
                  <div class="amount"><em class="icon ni ni-users"></em> {{ number_format($countYT) }}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>


  <div class="nk-block">
    <div class="card card-full">
      <div class="card-inner">
        
        <div class="nk-block-head nk-block-head-sm">
          <div class="nk-block-between">
            <div class="nk-block-head-content">
              <h4 class="nk-block-title page-title">Filter Chart:</h4>
            </div>

            <div class="nk-block-head-content">
              <div class="toggle-wrap nk-block-tools-toggle">
                <div class="toggle-expand-content" data-content="more-options">
                  <form action="{{ url()->current() }}" method="get">
                    <ul class="nk-block-tools g-1">
                      <li>
                        <div class="form-control-wrap">
                          <div class="form-icon form-icon-right">
                            <em class="icon ni ni-search"></em>
                          </div>
                          <input type="text" class="form-control" id="default-04" name="search" value="{{ isset($_GET['search']) ? $_GET['search'] : "" }}" placeholder="Cari Berdasarkan Nama">
                        </div>
                      </li>
                      <li>
                        <div class="form-control-wrap">
                          <div class="form-icon form-icon-right">
                            <em class="icon ni ni-search"></em>
                          </div>
                          <select class="form-select role yyyyy" name="sosmed" data-placeholder="Pilih Sosial Media">
                            <option label="empty" value=""></option>
                            <option @if (isset($_GET['sosmed']) && $_GET['sosmed'] === 'account_ig') selected @endif value="account_ig">Instagram</option>
                            <option @if (isset($_GET['sosmed']) && $_GET['sosmed'] === 'account_twt') selected @endif value="account_twt">Twitter</option>
                            <option @if (isset($_GET['sosmed']) && $_GET['sosmed'] === 'account_yt') selected @endif value="account_yt">Youtube</option>
                            <option @if (isset($_GET['sosmed']) && $_GET['sosmed'] === 'account_fb') selected @endif value="account_fb">Facebook</option>
                          </select>
                        </div>
                      </li>
                      <li>
                        <div class="form-control-wrap">
                          <div class="form-icon form-icon-right">
                            <em class="icon ni ni-search"></em>
                          </div>
                          <select class="form-select role yyyyy" name="orderBy" data-placeholder="Urutkan Berdasarkan">
                            <option label="empty" value=""></option>
                            <option @if (isset($_GET['orderBy']) && $_GET['orderBy'] === 'desc') selected @endif value="desc">Terbanyak</option>
                            <option @if (isset($_GET['orderBy']) && $_GET['orderBy'] === 'asc') selected @endif value="asc">Terendah</option>
                          </select>
                        </div>
                      </li>
                      <li class="nk-block-tools-opt">
                        <button class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-search"></em><span>Cari</span></button>
                      </li>
                    </ul>
                    
                  </form>
                </div>
        
              </div>
            </div>
            
          </div>
        </div>

      </div>
    </div>
  </div>
  
  @include('dashboards.partial.chartPost')
  @include('dashboards.partial.chartFollowers')

<style>
  canvas{
    max-width: inherit;
  }
  .chartWrapper {
    position: relative;
    overflow-x: scroll;
  }
  .chartWrapper > canvas {
      position: absolute;
      left: 0;
      top: 0;
      pointer-events:none;
  }
  .chartAreaWrapper {
    width: 100%;
    height: 600px;
    overflow-x: scroll;
  }
</style>

@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-zoom@0.7.7"></script>
<script>
  // console.log(222, @json( array_values($activity[1]) ));
  window.onload = function() {
    var ctx = document.getElementById("activityChart").getContext("2d");
    window.totalPost = new Chart(ctx, {
      type: "horizontalBar",
      data: {
        labels: @json($labelAct),
        datasets: [
          {
            label: 'Kampung Tertib Lalu Lintas',
            stack: 'Post',
            data: @json(array_values($activity[1])),
            backgroundColor: "#e720f6",
          },
          {
            label: 'Smart City',
            stack: 'Post',
            data: @json(array_values($activity[2])),
            backgroundColor: "#459013",
            borderSkipped: 'top'
          },
          {
            label: 'Taman Lantas',
            stack: 'Post',
            data: @json(array_values($activity[3])),
            backgroundColor: "#4a9dc8",
          },
          {
            label: 'PKS',
            stack: 'Post',
            data: @json(array_values($activity[4])),
            backgroundColor: "#0d06c6",
          },
          
          {
            label: 'Cara Aman ke Sekolah',
            stack: 'Post',
            data: @json(array_values($activity[5])),
            backgroundColor: "#9243e7",
          },

          {
            label: 'Masdarwis',
            stack: 'Post',
            data: @json(array_values($activity[6])),
            backgroundColor: "#fbbdbf",
          },
          
          {
            label: 'Pocil',
            stack: 'Post',
            data: @json(array_values($activity[7])),
            backgroundColor: "#f545b5",
          },
          
          {
            label: 'Kampus Pelopor Tertib Berlalu Lintas',
            stack: 'Post',
            data: @json(array_values($activity[8])),
            backgroundColor: "#5273aa",
          },
          {
            label: 'Pembinaan Komunitas',
            stack: 'Post',
            data: @json(array_values($activity[9])),
            backgroundColor: "#aa3c24",
          },
          {
            label: 'Pameran',
            stack: 'Post',
            data: @json(array_values($activity[10])),
            backgroundColor: "#7a15ec",
          },
          {
            label: 'Manajemen Media',
            stack: 'Post',
            data: @json(array_values($activity[11])),
            backgroundColor: "#b5b2b8",
          },
          {
            label: '7 Program Prioritas',
            stack: 'Post',
            data: @json(array_values($activity[12])),
            backgroundColor: "#299fa3",
          },
          {
            label: 'Indonesia Layak Anak (Idola)',
            stack: 'Post',
            data: @json(array_values($activity[13])),
            backgroundColor: "#023147",
          },
        ],
      },
      options: {
        maintainAspectRatio: @json($totalAcc) < 20 ? false : true,
        cutoutPercentage: 90,
        animation: {
          onComplete: function (animate) {
            var sourceCanvas = this.chart.ctx.canvas;
            var copyWidth = animate.chart.scales['x-axis-0'] - 5;
            var copyHeight = 1000;
            var targetCtx = document.getElementById("myChartAxisAct").getContext("2d");
            targetCtx.canvas.height = copyWidth;
            targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth, copyHeight, 0, 0, copyWidth, copyHeight);
          },
        },
        showXAxisLabel: {
          display: true,
        },
        title: {
          display: false,
          text: 'Total Post Sosial Media'
        },
        tooltips: {
          mode: 'index',
          intersect: false
        },
        responsive: true,
        scales: {
          yAxes: [{
            ticks: {
              stacked: true,
            }
          }],
          xAxes: [{
            ticks: {
              fontSize: 10,
            }
          }],
        },
        plugins: {
          zoom: {
            pan: {
              drag: true,
              mode: 'y',
              speed: 20,
            },
            zoom: {
              sensitivity: 3,
              speed: 20,
              mode: 'y',
              rangeMin: {
              },
            }
          }
        }
      }
    });

    var ctx = document.getElementById("igMedias").getContext("2d");
    window.totalPost = new Chart(ctx, {
      type: "horizontalBar",
      data: {
        labels: @json($label),
        datasets: [
          {
            label: 'Instagram',
            stack: 'Post',
            data: @json($data_post_ig),
            backgroundColor: "#c32aa3",
          },
          {
            label: 'Facebook',
            stack: 'Post',
            data: @json($data_post_fb),
            backgroundColor: "#1877f2",
            borderSkipped: 'top'
          },
          {
            label: 'Twitter',
            stack: 'Post',
            data: @json($data_post_twt),
            backgroundColor: "#1da1f2",
          },
          {
            label: 'Youtube',
            stack: 'Post',
            data: @json($data_post_yt),
            backgroundColor: "#ff0000",
          },
        ],
      },
      options: {
        maintainAspectRatio: @json($totalAcc) < 20 ? false : true,
        cutoutPercentage: 90,
        animation: {
          onComplete: function (animate) {
            var sourceCanvas = this.chart.ctx.canvas;
            var copyWidth = animate.chart.scales['x-axis-0'] - 5;
            var copyHeight = 1000;
            var targetCtx = document.getElementById("myChartAxis").getContext("2d");
            targetCtx.canvas.height = copyWidth;
            targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth, copyHeight, 0, 0, copyWidth, copyHeight);
          },
        },
        showXAxisLabel: {
          display: true,
        },
        title: {
          display: false,
          text: 'Total Post Sosial Media'
        },
        tooltips: {
          mode: 'index',
          intersect: false
        },
        responsive: true,
        scales: {
          yAxes: [{
            ticks: {
              stacked: true,
            }
          }],
          xAxes: [{
            ticks: {
              fontSize: 10,
            }
          }],
        },
        plugins: {
          zoom: {
            pan: {
              drag: true,
              mode: 'y',
              speed: 20,
            },
            zoom: {
              sensitivity: 3,
              speed: 20,
              mode: 'y',
              rangeMin: {
              },
            }
          }
        }
      }
    });
   

    var ctxx = document.getElementById("followers").getContext("2d");
    window.totalPost = new Chart(ctxx, {
      type: "horizontalBar",
      data: {
        labels: @json($label),
        datasets: [
          {
            label: 'Instagram',
            stack: 'Post',
            data: @json($data_followers_ig),
            backgroundColor: "#c32aa3",
          },
          {
            label: 'Facebook',
            stack: 'Post',
            data: @json($data_followers_fb),
            backgroundColor: "#1877f2",
            fill: false,
            pointHoverRadius: 10,
          },
          {
            label: 'Twitter',
            stack: 'Post',
            data: @json($data_followers_twt),
            backgroundColor: "#1da1f2",
            fill: false,
            pointHoverRadius: 10,
          },
          {
            label: 'Youtube',
            stack: 'Post',
            data: @json($data_followers_yt),
            backgroundColor: "#ff0000",
            fill: false,
            pointHoverRadius: 10,
          },
        ],
      },
      options: {
        maintainAspectRatio: @json($totalAcc) < 20 ? false : true,
        cutoutPercentage: 90,
        animation: {
          onComplete: function (animate) {
            var sourceCanvas = this.chart.ctx.canvas;
            var copyWidth = animate.chart.scales['x-axis-0'] - 5;
            var copyHeight = 1000;
            var targetCtx = document.getElementById("myChartAxis").getContext("2d");
            targetCtx.canvas.height = copyWidth;
            targetCtx.drawImage(sourceCanvas, 0, 0, copyWidth, copyHeight, 0, 0, copyWidth, copyHeight);
          },
        },
        showXAxisLabel: {
          display: true,
        },
        title: {
          display: false,
          text: 'Total Post Sosial Media'
        },
        tooltips: {
          mode: 'index',
          intersect: false
        },
        responsive: true,
        scales: {
          yAxes: [{
            ticks: {
              stacked: true,
            }
          }],
          xAxes: [{
            ticks: {
              fontSize: 10,
            }
          }],
        },
        plugins: {
          zoom: {
            pan: {
              drag: true,
              mode: 'y',
              speed: 20,
            },
            zoom: {
              sensitivity: 3,
              speed: 20,
              mode: 'y',
              rangeMin: {
              },
            }
          }
        }
      }
    });
  };
</script>

@endsection