@extends('layouts.app')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
          <div class="nk-block-head-content">
            <h3 class="nk-block-title page-title">{{ $title }}</h3>
          </div>
          <div class="nk-block-head-content">
            <div class="toggle-wrap nk-block-tools-toggle">
                <a href="{{ route('create_account') }}"><button class="btn btn-primary">Tambah Akun</button></a>
            </div>
          </div>
        </div>
    </div>
    
    
    <div class="nk-block">
      <div class="col-12">
        <canvas id="igMedias" style="display: block; height: 300px; width: 100%;" width="912" height="456" class="chartjs-render-monitor"></canvas>
      </div>
    </div>

    {{-- <h3 class="text-center mt-3">YouTube</h3> --}}

    {{-- <div class="row">
        <div class="col-md-6 col-12">
            <canvas id="ytVids" style="display: block; height: 100%; width: 100%;" width="912" height="456" class="chartjs-render-monitor"></canvas>
        </div>
        <div class="col-md-6 col-12">
            <canvas id="ytSubs" style="display: block; height: 100%; width: 100%;" width="912" height="456" class="chartjs-render-monitor"></canvas>
        </div>
    </div> --}}

@endsection

@section('script')

<script>
    window.onload = function() {
        var ctx = document.getElementById('igMedias').getContext('2d');
        // var ctx2 = document.getElementById('igFollowers').getContext('2d');

        window.totalPost = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?php echo $accLabel ?>,
                datasets: [
                    {
                        label: "<?php echo $igDatasets[0] ?>",
                        backgroundColor: '#004b6e',
                        borderWidth: 0,
                        data:  <?php echo $media ?>
                    },
                    {
                        label: "<?php echo $igDatasets[2] ?>",
                        backgroundColor: '#0479b0',
                        borderWidth: 0,
                        data:  <?php echo $followers ?>
                    },
                    {
                        label: "<?php echo $igDatasets[1] ?>",
                        backgroundColor: '#0095db',
                        borderWidth: 0,
                        data:  <?php echo $following ?>
                    },
                ]

            },
            options: {
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true,
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                responsive: true
            }
        });

        // window.totalFollow = new Chart(ctx2, {
        //     type: 'bar',
        //     data: {
        //         labels: <?php echo $accLabel ?>,
        //         datasets: [
        //             {
        //                 label: "<?php echo $igDatasets[2] ?>",
        //                 backgroundColor: '#003985',
        //                 borderWidth: 0,
        //                 data:  <?php echo $followers ?>
        //             },
        //         ]

        //     },
        //     options: {
        //         scales: {
        //             yAxes: [{
        //                 ticks: {
        //                     beginAtZero: true
        //                 }
        //             }]
        //         },
        //         responsive: true
        //     }
        // });
    }
</script>
@endsection
