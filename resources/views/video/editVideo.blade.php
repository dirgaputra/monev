@extends('layouts.app')
@section('content')
  @if (session('error'))
    <div class="alert alert-danger" role="alert">{{ session('error') }}</div>
  @endif
  @if (session('status'))
    <div class="alert alert-success" role="alert">{{ session('status') }}</div>
  @endif

 
  <div class="card card-preview col-5 offset-3 mt-5">
    
    <div class="card-inner">
      <div class="preview-block">
        
        <div class="nk-block-head nk-block-head-sm mb-4">
          <div class="nk-block-between">
            <div class="nk-block-head-content">
              <h3 class="nk-block-title page-title">{{ $title }}</h3>
            </div>
          </div>
        </div>
        
        <form action="" method="post" class="form-validate" enctype="multipart/form-data">
          @csrf
          <div class="row gy-4">
            
            <div class="col-sm-12">
              <div class="form-group">
                <label class="form-label" for="default-01">Judul Video</label>
                <div class="form-control-wrap">
                <input type="text" class="form-control" placeholder="Judul Video" name="title" value="{{$video->title}}" required autofocus>
                </div>
              </div>
            </div>
            
            <div class="col-sm-12">
              <div class="form-group">
                <label class="form-label" for="default-06">Video File</label>
                <div class="form-control-wrap">
                  <div class="custom-file">
                    <input type="file" name="file" class="custom-file-input" id="customFile">
                    <label class="custom-file-label" for="customFile">{{$video->fileName}}</label>
                  </div>
                </div>
              </div>
            </div>
            
            <div class="col-md-12 mt-5">
              <div class="form-group">
                <button type="submit" class="btn btn-lg btn-primary mr-3">Kirim</button>
                <a href="{{url('/video')}}" class="btn btn-lg btn-danger">Cancel</a>
              </div>
            </div>

          </div>

        </form>
      </div>
    </div>
  </div>
@endsection
