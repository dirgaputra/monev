@extends('layouts.app')
@section('content')
  @if (session('status'))
    <div class="alert alert-success" role="alert">{{ session('status') }}</div>
  @endif
  @if (session('error'))
    <div class="alert alert-danger" role="alert">{{ session('error') }}</div>
  @endif
  <div class="nk-block-head nk-block-head-sm">
    <div class="nk-block-between">
      <div class="nk-block-head-content">
        <h3 class="nk-block-title page-title">{{ $title }} ({{$total}})</h3>
      </div>
      <div class="nk-block-head-content">
        <div class="toggle-wrap nk-block-tools-toggle">
          <a href="{{ route('create_video') }}"><button class="btn btn-primary">Tambah Video</button></a>
        </div>
      </div>
    </div>
  </div>

  <div class="nk-block">
    <div class="nk-tb-list is-separate mb-3">
      <div class="nk-tb-item nk-tb-head">
        <div class="nk-tb-col"><span class="sub-text">Judul</span></div>
        <div class="nk-tb-col"><span class="sub-text">Video</span></div>
        <div class="nk-tb-col"><span class="sub-text">Nama File</span></div>
        <div class="nk-tb-col"><span class="sub-text">Nama Author</span></div>
        <div class="nk-tb-col"><span class="sub-text">Tanggal Dibuat</span></div>
        <div class="nk-tb-col">&nbsp;</div>
      </div>
      @foreach ($video as $_video)
        <div class="nk-tb-item _list_{{ $_video->uid }}">
          <div class="nk-tb-col">
            <span>{{$_video->title}}</span>
          </div>
          <div class="nk-tb-col">
            {{-- <img src="{{url('/'.$_video->path)}}" width="50" alt="{{$_video->fileName}}"> --}}
            @if ($_video->path ) <em class="icon ni ni-check-circle-fill text-success title h4"></em> @else <em class="icon ni ni-cross-circle-fill text-danger title h4"></em> @endif
          </div>
          <div class="nk-tb-col">
            <span class="tb-amount">{{$_video->fileName }}</span>
          </div>
          <div class="nk-tb-col">
            <span class="tb-amount">{{$_video->user ? $_video->user->name : '-'}}</span>
          </div>
          <div class="nk-tb-col">
            <span>{{ date("D, d M Y", strtotime($_video->created_at)) }}</span>
          </div>
          <div class="nk-tb-col nk-tb-col-tools">
            <div class="dropdown">
              <a class="text-soft dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown" data-offset="-8,0"><em class="icon ni ni-more-h"></em></a>
              <div class="dropdown-menu dropdown-menu-right dropdown-menu-xs">
                <ul class="link-list-plain">
                  <li><a href="{{ url('/video/edit/'.$_video->uid) }}" class="text-primary">Ubah Akun</a></li>
                  <li><a href="javascript:;" attr="{{ $_video->uid }}" class="remove_sosmed text-danger">Hapus Akun</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
    <div class="card">
      <div class="card-inner">
        {{ $video->links() }}
      </div>
    </div>
  </div>

@endsection


@section('script')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
    $(document).ready( function () {
      $('.remove_sosmed').on("click", function (e) {      
        var attr = $(this).attr('attr');
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
          if (result.value) {
            $.ajax({
              method: 'GET',
              url: `${window.location.origin}/video/delete`,
              dataType: 'json',
              data: { 'id': attr },
              success: function(result){
                $('._list_'+attr).remove();
                return Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
              }
            });
          }
        });
      });
    });
  </script>
@endsection
