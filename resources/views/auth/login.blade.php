@extends('layouts.appAuth') 
@section('content')
<div class="nk-wrap nk-wrap-nosidebar">
  <div class="nk-split nk-split-page nk-split-md bg-black">
    <div class="nk-split-content nk-split-stretch bg-lighter d-flex toggle-break-lg toggle-slide toggle-slide-right" data-content="athPromo" data-toggle-screen="lg" data-toggle-overlay="true">
      
      <div id="carouselExConInd" class="carousel slide w-100 m-auto" data-ride="carousel">
        <div class="carousel-inner">
          @foreach (App\Slider::where('type', 'login')->get() as $index => $item)
            <div class="carousel-item @if($index === 0) active @endif">
              <div class="nk-split-content nk-split-stretch" style="width: 100%; height: 100vh; background: url('{{asset($item->path)}}') top center; background-size: cover "></div>
            </div>
          @endforeach
          <ol class="carousel-indicators">
            @foreach (App\Slider::where('type', 'login')->get() as $index => $item)
              <li data-target="#carouselExConInd" data-slide-to="{{$index}}" class="@if($index === 0) active @endif"></li>
            @endforeach
          </ol>
        </div>
      </div>
    </div>
    
    <div class="nk-split-content nk-block-area nk-block-area-column nk-auth-container bg-white">
      <div class="nk-block nk-block-middle nk-auth-body">
        <div class="brand-logo pb-5">
          <a href={{url('/login')}} class="logo-link">
            <img class="logo-dark logo-img logo-img-lg" src="{{asset('/images/korlantas.png')}}" alt="logo-dark" />
          </a>
        </div>
        <div class="nk-block-head">
          <div class="nk-block-head-content">
            <h5 class="nk-block-title">Masuk Monev</h5>
            <div class="nk-block-des">
              <p>Silahkan login untuk masuk pada sistem</p>
            </div>
          </div>
        </div>
        @error('email') 
          <div class="alert alert-danger" role="alert">Email atau Password anda salah!</div>
        @enderror
        <form method="POST" action="{{ route('login') }}" autocomplete="off">
          @csrf
          <div class="form-group">
            <div class="form-label-group">
              <label class="form-label" for="default-01">Email</label>
            </div>
            <input type="text" class="form-control form-control-lg" id="default-01" placeholder="me@example.com"  name="email" autofocus/>
          </div>
          <div class="form-group">
            <div class="form-label-group">
              <label class="form-label" for="password">Password</label>
            </div>
            <div class="form-control-wrap">
              <a tabindex="-1" href="#" class="form-icon form-icon-right passcode-switch" data-target="password">
                <em class="passcode-icon icon-show icon ni ni-eye"></em>
                <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
              </a>
              <input type="password" class="form-control form-control-lg" id="password" name="password"  placeholder="Password" />
            </div>
          </div>
          <div class="form-group">
            <button class="btn btn-lg btn-primary btn-block">MASUK</button>
          </div>
        </form>
      </div>

    </div>
    
  </div>
    
</div>
@endsection
