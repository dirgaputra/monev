@extends('layouts.app')
@section('content')
@include('layouts.headerActivity')

  <div class="nk-block">
    <div class="nk-tb-list is-separate mb-3">
      <div class="nk-tb-item nk-tb-head">
        <div class="nk-tb-col"><span class="sub-text">Nama Author</span></div>
        <div class="nk-tb-col"><span class="sub-text">Role</span></div>
        <div class="nk-tb-col"><span class="sub-text">Jenis Aktifitas</span></div>
        <div class="nk-tb-col"><span class="sub-text">Target</span></div>
        <div class="nk-tb-col"><span class="sub-text">Pelaksanaan</span></div>
        <div class="nk-tb-col"><span class="sub-text">Peserta</span></div>
        <div class="nk-tb-col"><span class="sub-text">Dokumentasi</span></div>
        <div class="nk-tb-col"><span class="sub-text">Tipe</span></div>
        <div class="nk-tb-col">&nbsp;</div>
      </div>
      @foreach ($activity as $_activity)
        <div class="nk-tb-item _list_{{ $_activity->uid }}">
          <div class="nk-tb-col">
            <span class="tb-amount">{{$_activity->user->name}}</span>
          </div>
          <div class="nk-tb-col">
            <span>{{ $_activity->user->role->role_name }}</span>
            {{-- <span>{{ $_activity->user->role_id }}</span> --}}
          </div>
          <div class="nk-tb-col">
            <span>{{ $_activity->activity_master->name }}</span>
          </div>
          <div class="nk-tb-col">
            <span>{{ date("D, d M Y", strtotime($_activity->target)) }}</span>
          </div>
          <div class="nk-tb-col">
            <span>{{ date("D, d M Y", strtotime($_activity->pelaksanaan)) }}</span>
          </div>
          <div class="nk-tb-col">
            <span>{{ $_activity->peserta }}</span>
          </div>
          <div class="nk-tb-col text-center">
            @if ($_activity->dokumentasi ) <em class="icon ni ni-check-circle-fill text-success title h4"></em> @else <em class="icon ni ni-cross-circle-fill text-danger title h4"></em> @endif
          </div>
          <div class="nk-tb-col text-center">
            <span style="text-transform: capitalize">{{ $_activity->type }}</span>
          </div>
          <div class="nk-tb-col nk-tb-col-tools">
            <div class="dropdown">
              <a class="text-soft dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown" data-offset="-8,0"><em class="icon ni ni-more-h"></em></a>
              <div class="dropdown-menu dropdown-menu-right dropdown-menu-sm">
                <ul class="link-list-plain">
                  <li><a href="{{ url('/activity/detail/'.$_activity->uid) }}" class="text-primary">Lihat Aktifitas</a></li>
                  <li><a href="{{ url('/activity/edit/'.$_activity->uid) }}" class="text-primary">Ubah Aktifitas</a></li>
                  <li><a href="javascript:;" attr="{{ $_activity->uid }}" class="remove_sosmed text-danger">Hapus Aktifitas</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
    <div class="card">
      <div class="card-inner">
        {{ $activity->links() }}
      </div>
    </div>
  </div>

@endsection


@section('script')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
    $(document).ready( function () {
      $('.remove_sosmed').on("click", function (e) {      
        var attr = $(this).attr('attr');
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
          if (result.value) {
            $.ajax({
              method: 'GET',
              url: `${window.location.origin}/activity/delete`,
              dataType: 'json',
              data: { 'id': attr },
              success: function(result){
                $('._list_'+attr).remove();
                return Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
              }
            });
          }
        });
      });
    });
  </script>
@endsection
