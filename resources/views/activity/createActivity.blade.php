@extends('layouts.app')
@section('content')
  @if (session('status'))
    <div class="alert alert-success" role="alert">{{ session('status') }}</div>
  @endif
  @if (session('error'))
    <div class="alert alert-danger" role="alert">{{ session('error') }}</div>
  @endif
 
  <div class="nk-block-head nk-block-head-sm">
    <div class="nk-block-between">
      <div class="nk-block-head-content">
        <h3 class="nk-block-title page-title">{{ $title }}</h3>
      </div>
    </div>
  </div>
  <div class="card card-preview">
    <div class="card-inner">
      <div class="preview-block">
        
        <form action="" method="post" class="form-validate" enctype="multipart/form-data">
          @csrf
          <div class="row gy-4">
            @if (Auth::user()->role_id === 1)
              <div class="col-sm-12">
                <div class="form-group">
                  <label class="form-label" for="default-01">User Author</label>
                  <div class="form-control-wrap">
                    <select class="form-select role yyyyy" name="user_id" required data-placeholder="Pilih User Author"  data-search="on">
                      <option label="empty" value=""></option>
                      @foreach ($users as $_users)
                        <option value="{{$_users->uuid}}">{{$_users->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
            @endif
            
            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label" for="default-01">Jenis Aktifitas</label>
                <div class="form-control-wrap">
                  <select class="form-select role yyyyy" name="activity_id" required data-placeholder="Jenis Aktifitas" data-search="on">
                    <option label="empty" value=""></option>
                    @foreach ($activity_master as $_activity_master)
                      <option value="{{$_activity_master->id}}">{{$_activity_master->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label">Target</label>
                <div class="form-control-wrap">
                  <div class="form-icon form-icon-left"><em class="icon ni ni-calendar"></em></div>
                  <input type="text" class="form-control date-picker" name="target" data-date-format="yyyy-mm-dd" required>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label">Pelaksanaan</label>
                <div class="form-control-wrap">
                  <div class="form-icon form-icon-left"><em class="icon ni ni-calendar"></em></div>
                  <input type="text" class="form-control date-picker" name="pelaksanaan" data-date-format="yyyy-mm-dd" required>
                </div>
              </div>
            </div>
            
            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label" for="default-01">Peserta</label>
                <div class="form-control-wrap">
                  <input type="text" class="form-control" placeholder="Peserta" name="peserta" required>
                </div>
              </div>
            </div>
            
            <div class="col-sm-6">
              <div class="form-group">
                <label class="form-label" for="default-01">Dokumentasi</label>
                <div class="form-control-wrap">
                  <ul class="custom-control-group g-3 align-center flex-wrap">
                    <li>
                      <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" checked name="type" value="file" id="reg-enable">
                        <label class="custom-control-label" for="reg-enable">Foto / Pdf</label>
                      </div>
                    </li>
                    <li>
                      <div class="custom-control custom-radio">
                        <input type="radio" class="custom-control-input" name="type" id="reg-disable" value="link">
                        <label class="custom-control-label" for="reg-disable">URL Youtube</label>
                      </div>
                    </li>
                  </ul>
                </div>
                
                <div class="formUpload mt-3">
                  <div class="form-control-wrap">
                    <input type="file" name="dokumentasi" class="custom-file-input" required>
                    <label class="custom-file-label" for="customFile">Pilih file</label>
                  </div>
                  <div class="form-note">File format <code>(PDF, JPG & PNG)</code></div>
                </div>
              </div>
            </div>
          
            <div class="col-md-12 mt-2">
              <div class="form-group">
                <button type="submit" class="btn btn-lg btn-primary mr-3">Kirim</button>
                <a href="{{url('/user')}}" class="btn btn-lg btn-danger">Cancel</a>
              </div>
            </div>

          </div>

        </form>
      </div>
    </div>
  </div>
@endsection
@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
  $(document).ready( function () {
    $("input[name='type']").change(function(){
      var val = this.value;
      console.log(val)
      var htmlFile = '<div class="form-control-wrap"> <input type="file" name="dokumentasi" class="custom-file-input" required> <label class="custom-file-label" for="customFile">Pilih file</label> </div> <div class="form-note">File format <code>(PDF, JPG & PNG)</code></div>'
      var htmlLink = '<div class="form-control-wrap"> <input type="text" name="dokumentasi" class="form-control" placeholder="URL Youtube" required>  </div> <div class="form-note">URL Youtube (https://www.youtube.com/watch?v=<code>{ID_VIDEO}</code>)</div>'
      if(val === 'file'){
        $('.formUpload').html(htmlFile);
      }else{
        $('.formUpload').html(htmlLink);
      }
    });
  });
</script>
@endsection


