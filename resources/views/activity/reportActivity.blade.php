<table>
  <tbody>
    <tr>
      <th colspan="3" style="font-weight: bold">Name</th>
      <th colspan="2" style="text-align: left">Report {{$title}}</th>
    </tr>
    <tr>
      <th colspan="3" style="font-weight: bold">Date</th>
      <th colspan="2" style="text-align: left">
        {{  date("l, d F Y - H:m:s", strtotime($date))  }}
      </th>
    </tr>
  </tbody>
</table>

<table>
  <thead>
    <tr>
      <th style="background-color: black; color: white; font-weight: bold">NO</th>
      <th style="background-color: black; color: white; font-weight: bold">NAMA</th>
      <th style="background-color: black; color: white; font-weight: bold">ROLE</th>
      <th style="background-color: black; color: white; font-weight: bold">JENIS AKTIFITAS</th>
      <th style="background-color: black; color: white; font-weight: bold">TARGET</th>
      <th style="background-color: black; color: white; font-weight: bold">PELAKSANAAN</th>
      <th style="background-color: black; color: white; font-weight: bold">PESERTA</th>
      <th style="background-color: black; color: white; font-weight: bold">DOKUMENTASI</th>
      <th style="background-color: black; color: white; font-weight: bold">TYPE FILE</th>
      <th style="background-color: black; color: white; font-weight: bold">LINK</th>
    </tr>
  </thead>
  <tbody>
    @php $i=1 @endphp
		@foreach($report as $item)
      <tr>
        <td>{{ $i++ }}</td>
				<td width="40">{{$item->user ? $item->user->name : '-'}}</td>
        <td width="10">{{$item->user ? $item->user->role->role_name : '-'}}</td>
        <td width="40">{{$item->activity_master->name}}</td>
        <td width="40">{{ date("l, d F Y - H:m:s", strtotime($item->target)) }}</td>
        <td width="40">{{ date("l, d F Y - H:m:s", strtotime($item->pelaksanaan)) }}</td>
        <td width="20">{{$item->peserta}}</td>
        <td width="20" style="text-align: center">@if ($item->dokumentasi ) √ @else x @endif</td>
        <td width="20">{{$item->type}}</td>
        <td width="20">
          @if ($item->type === 'file')
            http://monev.rasirosakorlantas.id/{{$item->path}}
          @else
            https://www.youtube.com/watch?v={{$item->dokumentasi}}
          @endif
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
