@extends('layouts.app')
@section('content')
  <div class="nk-block-head nk-block-head-sm">
    <div class="nk-block-between">
      <div class="nk-block-head-content">
        <h3 class="nk-block-title page-title">{{ $title }}</h3>
        <div class="nk-block-des text-soft">
          <p>Detil Aktifitas <b>{{$_activity->user->role->role_name}} {{$_activity->user->name}}</b></p>
        </div>
      </div>
      <div class="nk-block-head-content">
        {{-- <div class="toggle-wrap nk-block-tools-toggle">
          <div class="toggle-expand-content" data-content="more-options">
            <form action="{{ url()->current() }}" method="get">
              <ul class="nk-block-tools g-3">
                <li class="nk-block-tools-opt">
                  <a href='{{url()->current() . '/export'}}' class="btn btn-secondary d-none d-md-inline-flex">
                    <em class="icon ni ni-download"></em>
                    <span>Export</span>
                  </a>
                </li>
              </ul>
            </form>
          </div>
        </div> --}}
      </div>
    </div>
  </div>

  <div class="nk-block">
    <div class="nk-tb-list is-separate mb-3">

      <div class="card">
        <div class="card-aside-wrap">
          <div class="card-content">
            <div class="card-inner">
              <div class="nk-block">
                <div class="nk-block-head">
                  <h5 class="title">Aktifitas Informasi</h5>
                </div>
                <div class="profile-ud-list">
                  <div class="profile-ud-item">
                    <div class="profile-ud wider">
                      <span class="profile-ud-label">Nama Author</span>
                      <span class="profile-ud-value">{{$_activity->user->name}}</span>
                    </div>
                  </div>
                  <div class="profile-ud-item">
                    <div class="profile-ud wider">
                      <span class="profile-ud-label">Role</span>
                      <span class="profile-ud-value">{{ $_activity->user->role->role_name }}</span>
                    </div>
                  </div>
                  <div class="profile-ud-item">
                    <div class="profile-ud wider">
                      <span class="profile-ud-label">Jenis Aktifitas</span>
                      <span class="profile-ud-value">{{ date("D, d M Y", strtotime($_activity->target)) }}</span>
                    </div>
                  </div>
                  <div class="profile-ud-item">
                    <div class="profile-ud wider">
                      <span class="profile-ud-label">Target</span>
                      <span class="profile-ud-value">{{ date("D, d M Y", strtotime($_activity->pelaksanaan)) }}</span>
                    </div>
                  </div>
                  <div class="profile-ud-item">
                    <div class="profile-ud wider">
                      <span class="profile-ud-label">Pelaksanaan</span>
                      <span class="profile-ud-value">{{ $_activity->peserta }}</span>
                    </div>
                  </div>
                  <div class="profile-ud-item">
                    <div class="profile-ud wider">
                      <span class="profile-ud-label">Dokumentasi</span>
                      <span class="profile-ud-value">{{ $_activity->type }}</span>
                    </div>
                  </div>
                </div>
              </div>
             
              <div class="nk-divider divider md"></div>
              <div class="nk-block">
                <div class="nk-block-head nk-block-head-sm nk-block-between">
                  <h5 class="title">Dokumentasi</h5>
                </div>
                <div class="bq-note">
                  <div class="bq-note-item">
                    <div class="bq-note-text text-center">
                      @if ($_activity->type === 'file')
                        <img src="https://monev.rasirosakorlantas.id/{{ $_activity->path }}">
                      @else
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$_activity->dokumentasi}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      
    </div>
  </div>

@endsection
