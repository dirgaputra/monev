@extends('layouts.app')
@section('content')
@include('layouts.header.Slider')

  <div class="nk-block">
    <div class="nk-tb-list is-separate mb-3">
      <div class="nk-tb-item nk-tb-head">
        <div class="nk-tb-col"><span class="sub-text"></span></div>
        <div class="nk-tb-col"><span class="sub-text">Nama Author</span></div>
        <div class="nk-tb-col"><span class="sub-text">Slider Name</span></div>
        <div class="nk-tb-col"><span class="sub-text">Tipe Slider</span></div>
        <div class="nk-tb-col"><span class="sub-text">Tanggal Dibuat</span></div>
        <div class="nk-tb-col">&nbsp;</div>
      </div>
      @foreach ($slider as $_slider)
        <div class="nk-tb-item _list_{{ $_slider->uid }}">
          <div class="nk-tb-col" style="width: 100px">
              <img src="{{$_slider->path}}" alt="">
          </div>
          <div class="nk-tb-col">
            <span class="tb-amount">{{$_slider->user->name}}</span>
          </div>
          <div class="nk-tb-col">
            <span>{{$_slider->fileName}}</span>
          </div>
          
          <div class="nk-tb-col">
            <span style="text-transform: capitalize">{{$_slider->type}}</span>
          </div>

          <div class="nk-tb-col">
            <span>{{ date("D, d M Y H:i:s", strtotime($_slider->created_at)) }}</span>
          </div>
          <div class="nk-tb-col nk-tb-col-tools">
            <div class="dropdown">
              <a class="text-soft dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown" data-offset="-8,0"><em class="icon ni ni-more-h"></em></a>
              <div class="dropdown-menu dropdown-menu-right dropdown-menu-xs">
                <ul class="link-list-plain">
                  <li><a href="{{ url('/slider/edit/'.$_slider->uid) }}" class="text-primary">Ubah Slider</a></li>
                  <li><a href="javascript:;" attr="{{ $_slider->uid }}" class="remove_sosmed text-danger">Hapus Slider</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
    <div class="card">
      <div class="card-inner">
        {{ $slider->links() }}
      </div>
    </div>
  </div>

@endsection


@section('script')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
    $(document).ready( function () {
      $('.remove_sosmed').on("click", function (e) {      
        var attr = $(this).attr('attr');
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
          if (result.value) {
            $.ajax({
              method: 'GET',
              url: `${window.location.origin}/slider/delete`,
              dataType: 'json',
              data: { 'id': attr },
              success: function(result){
                $('._list_'+attr).remove();
                return Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
              }
            });
          }
        });
      });
    });
  </script>
@endsection
