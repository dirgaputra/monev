@extends('layouts.app')
@section('content')
  @if (session('status'))
    <div class="alert alert-success" role="alert">{{ session('status') }}</div>
  @endif
  @if (session('error'))
    <div class="alert alert-danger" role="alert">{{ session('error') }}</div>
  @endif
 
  <div class="nk-block-head nk-block-head-sm">
    <div class="nk-block-between">
      <div class="nk-block-head-content">
        <h3 class="nk-block-title page-title">{{ $title }}</h3>
      </div>
    </div>
  </div>
  <div class="card card-preview">
    <div class="card-inner">
      <div class="preview-block">
        
        <form action="" method="post" class="form-validate" enctype="multipart/form-data">
          @csrf
          <div class="row gy-4">
            <div class="col-sm-12">
              <div class="form-group">
                <label class="form-label">Tipe Slider</label>
                <div class="form-control-wrap">
                  <select class="form-select role yyyyy" name="type" required data-placeholder="Pilih Tipe Slider">
                    <option label="empty" value=""></option>
                    <option value="infografis" @if ($slider->type === 'infografis' ) selected @endif>Infografis</option>
                    <option value="login" @if ($slider->type === 'login' ) selected @endif>Login Page</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <label class="form-label">Upload Image</label>
                <div class="form-control-wrap">
                  <div class="form-control-wrap">
                    <input type="file" name="slider" class="custom-file-input" required>
                    <label class="custom-file-label" for="customFile">{{$slider->fileName}}</label>
                  </div>
                  <div class="form-note">File Pixel <code>(1280 x 650 px)</code></div>
                </div>
              </div>
            </div>

            <div class="col-md-12 mt-5">
              <div class="form-group">
                <button type="submit" class="btn btn-lg btn-primary mr-3">Ubah</button>
                <a href="{{url('/user')}}" class="btn btn-lg btn-danger">Cancel</a>
              </div>
            </div>

          </div>

        </form>
      </div>
    </div>
  </div>
@endsection
