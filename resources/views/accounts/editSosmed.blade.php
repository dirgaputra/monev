@extends('layouts.app') 
@section('content') 
@if (session('status'))
  <div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif @if (session('error'))
  <div class="alert alert-danger" role="alert">
    {{ session('error') }}
  </div>
@endif
<div class="nk-block-head nk-block-head-sm">
  <div class="nk-block-between">
    <div class="nk-block-head-content">
      <h3 class="nk-block-title page-title">{{ $title }}</h3>
    </div>
  </div>
</div>
<div class="card card-preview">
  <div class="card-inner">
    <div class="container">
      <form action="" method="post" class="form-validate">
        @csrf
        
        <div class="row mb-5 align-center">
          <div class="col-lg-5">
            <div class="form-control-wrap">
              <label class="form-label">Pilih User</label>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="form-group">
              <div class="form-control-wrap">
                <select class="form-select role yyyyy" name="user_id" required data-placeholder="Pilih User" data-search="on">
                  <option label="empty" value=""></option>
                  @foreach ($users as $_users)
                    <option value="{{$_users->uuid}}" @if ($_users->uuid === $account->user->uuid ) selected @endif>{{$_users->name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
        </div>

        <div class="row mb-5 align-center">
          <div class="col-lg-5">
            <div class="form-control-wrap">
              <label class="form-label">Nama Akun</label>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="form-group">
              <div class="form-control-wrap">
                <input type="text" class="form-control" placeholder="Nama Akun" name="name" value="{{ $account->name }}" required />
              </div>
            </div>
          </div>
        </div>

        <div class="row mb-5 align-center">
          <div class="col-lg-5">
            <div class="form-control-wrap">
              <label class="form-label">Instagram</label>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="form-group">
              <div class="form-control-wrap">
                <input type="text" class="form-control" placeholder="Username tanpa @" name="ig"  value="{{$account->account_ig ? $account->account_ig->account : ''}}" />
              </div>
            </div>
          </div>
        </div>
        
        <div class="row mb-5 align-center">
          <div class="col-lg-5">
            <div class="form-control-wrap">
              <label class="form-label">Facebook</label>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="form-group">
              <div class="form-control-wrap">
                <input type="text" class="form-control" id="fb" name="fb" value="{{$account->account_fb ? $account->account_fb->account : ''}}"/>
                <p>
                  <i>facebook.com/<strong>{username}</strong></i>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row mb-5 align-center">
          <div class="col-lg-5">
            <div class="form-control-wrap">
              <label class="form-label">Twitter</label>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="form-group">
              <div class="form-control-wrap">
                <input type="text"  class="form-control" placeholder="Username tanpa @" name="tw" value="{{$account->account_twt ? $account->account_twt->account : ''}}" />
                <p>
                  <i>twitter.com/<strong>{username}</strong></i>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row mb-5 align-center">
          <div class="col-lg-5">
            <div class="form-control-wrap">
              <label class="form-label">YouTube</label>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="form-group">
              <div class="form-control-wrap">
                <input type="text" class="form-control" id="yt" name="yt" value="{{$account->account_yt ? $account->account_yt->account : ''}}"/>
                <p>
                  <i>youtube.com/channel/<strong>{id channel}</strong></i>
                </p>
              </div>
            </div>
          </div>
        </div>
        {{$account->uid}}
        <div class="row">
          <div class="col-lg-3 offset-lg-10">
            <div class="form-group mt-5">
              <input type="text" value="{{url()->previous()}}" class="form-control" hidden name="url" />
              <input type="text" value="{{$account->uid}}" class="form-control"  hidden name="account_id" />
              <button type="submit" class="btn btn-lg btn-primary">Ubah Data</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection 
