@extends('layouts.app')
@section('content')
@include('layouts.header')

  <div class="nk-block">
    <div class="nk-tb-list is-separate mb-3">
      <div class="nk-tb-item nk-tb-head">
        <div class="nk-tb-col"><span class="sub-text">Nama</span></div>
        <div class="nk-tb-col"><span class="sub-text">Nama Author</span></div>
        <div class="nk-tb-col"><span class="sub-text">Role</span></div>
        <div class="nk-tb-col"><span class="sub-text">Nama Akun</span></div>
        <div class="nk-tb-col text-center"><span class="sub-text">Tweet</span></div>
        <div class="nk-tb-col text-center"><span class="sub-text">Pengikut</span></div>
        <div class="nk-tb-col text-center"><span class="sub-text">Diikuti</span></div>
        <div class="nk-tb-col">&nbsp;</div>
      </div>
      @foreach ($accounts as $_accounts)
        <div class="nk-tb-item _list_{{ $_accounts->id }}">
          <div class="nk-tb-col">
            <span class="tb-amount">{{ $_accounts->accounts ? $_accounts->accounts->name : '-'}}</span>
          </div>
          <div class="nk-tb-col">
            {{$_accounts->accounts ? $_accounts->accounts->user->name: '-'}}
          </div>
          <div class="nk-tb-col">
            {{$_accounts->accounts ? $_accounts->accounts->user->role->role_name: '-'}}
          </div>
          <div class="nk-tb-col">
            <span> {{ '@'.$_accounts->account }}</span>
          </div>
          <div class="nk-tb-col text-center _post_{{str_replace('.', '_', $_accounts->account)}}">
            <span>{{number_format($_accounts->post)}}</span>
          </div>
          <div class="nk-tb-col text-center _flwng_{{str_replace('.', '_', $_accounts->account)}}">
            <span>{{ number_format($_accounts->following) }}</span>
          </div>
          <div class="nk-tb-col text-center _flwrs_{{str_replace('.', '_', $_accounts->account)}}">
            <span>{{ number_format($_accounts->followers) }}</span>
          </div>
          <div class="nk-tb-col nk-tb-col-tools">
            <div class="dropdown">
              <a class="text-soft dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown" data-offset="-8,0"><em class="icon ni ni-more-h"></em></a>
              <div class="dropdown-menu dropdown-menu-right dropdown-menu-xs">
                <ul class="link-list-plain">
                  <li><a href="{{ url('/account/edit/'.$_accounts->user_account) }}" class="text-primary">Ubah Akun</a></li>
                  <li><a href="https://twitter.com/{{$_accounts->account}}" target="_blank"  class="text-primary">Lihat Akun</a></li>
                  <li><a href="javascript:;" attr="{{ $_accounts->id }}" class="remove_sosmed text-danger">Hapus Akun</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
    <div class="card">
      <div class="card-inner">
        {{ $accounts->links() }}
      </div>
    </div>
  </div>

@endsection


@section('script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready( function () {
  $('.remove_sosmed').on("click", function (e) {      
    var attr = $(this).attr('attr');
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!'
    }).then(function (result) {
      if (result.value) {
        $.ajax({
          method: 'GET',
          url: `${window.location.origin}/account/remove`,
          dataType: 'json',
          data: { 'id': attr, 'type': 'account_twts' },
          success: function(result){
            $('._list_'+attr).remove();
            return Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
          }
        });
      }
    });
  });
  
  @foreach ($accounts as $_accounts)
    $.ajax({
      method: 'GET',
      url: `${window.location.origin}/getTWT`,
      dataType: 'json',
      data: { account: "{{$_accounts->account}}"},
      success: function(result){
        $('._post_'+result.account).html(result.post)
        $('._flwrs_'+result.account).html(result.followers)
        $('._flwng_'+result.account).html(result.following)
      }
    });
  @endforeach

});
</script>



@endsection
