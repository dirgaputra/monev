@extends('layouts.app') 
@section('content') 

@if (session('status'))
  <div class="alert alert-success" role="alert">{{ session('status') }}</div>
@endif @if (session('error'))
  <div class="alert alert-danger" role="alert">{{ session('error') }}</div>
@endif

<div class="nk-block-head nk-block-head-sm">
  <div class="nk-block-between">
    <div class="nk-block-head-content">
      <h3 class="nk-block-title page-title">{{ $title }}</h3>
    </div>
  </div>
</div>
<div class="card card-preview">
  <div class="card-inner">
    <div class="container">
      <form action="" method="post" class="gy-3">
        @csrf
        
        @if (Auth::user()->role_id === 1)
          <div class="row g-3 align-center">
            <div class="col-lg-5">
              <div class="custom-control custom-control">
                <label class="custom-label">Pilih User</label>
              </div>
            </div>
            <div class="col-lg-7">
              <div class="form-group">
                <div class="form-control-wrap">
                  <select class="form-select role yyyyy" name="user_id" required data-placeholder="Pilih User"  data-search="on">
                    <option label="empty" value=""></option>
                    @foreach ($users as $_users)
                      <option value="{{$_users->uuid}}">{{$_users->name}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>
        @endif
        
        <div class="row g-3 align-center">
          <div class="col-lg-5">
            <div class="custom-control custom-control">
              <label class="custom-label">Nama Akun</label>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="form-group">
              <div class="form-control-wrap">
                <input type="text" class="form-control" placeholder="Nama Akun" name="name" required />
              </div>
            </div>
          </div>
        </div>

        <div class="row g-3 align-center">
          <div class="col-lg-5">
            <div class="custom-control custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="ig-check" />
              <label class="custom-control-label" for="ig-check">Instagram</label>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="form-group">
              <div class="form-control-wrap">
                <input type="text" disabled class="form-control" placeholder="Username tanpa @" id="ig" name="ig" required />
              </div>
            </div>
          </div>
        </div>
        <div class="row g-3 align-center">
          <div class="col-lg-5">
            <div class="custom-control custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="fb-check" />
              <label class="custom-control-label" for="fb-check">Facebook</label>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="form-group">
              <div class="form-control-wrap">
                <input type="text" disabled class="form-control" id="fb" name="fb" required />
                <p>
                  <i>facebook.com/<strong>{username}</strong></i>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row g-3 align-center">
          <div class="col-lg-5">
            <div class="custom-control custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="tw-check" />
              <label class="custom-control-label" for="tw-check">Twitter</label>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="form-group">
              <div class="form-control-wrap">
                <input type="text" disabled class="form-control" placeholder="Username tanpa @" id="tw" name="tw" />
                <p>
                  <i>twitter.com/<strong>{username}</strong></i>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row g-3 align-center">
          <div class="col-lg-5">
            <div class="custom-control custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="yt-check" />
              <label class="custom-control-label" for="yt-check">YouTube</label>
            </div>
          </div>
          <div class="col-lg-7">
            <div class="form-group">
              <div class="form-control-wrap">
                <input type="text" disabled class="form-control" id="yt" name="yt" required />
                <p>
                  <i>youtube.com/channel/<strong>{id channel}</strong></i>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="row g-3">
          <div class="col-lg-3 offset-lg-10">
            <div class="form-group mt-5">
              <input type="text" value="{{url()->previous()}}" class="form-control" hidden name="url" />
              <button type="submit" class="btn btn-lg btn-primary">Kirim Data</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection @section('script')
<script>
  document.getElementById("ig-check").onchange = function () {
    document.getElementById("ig").disabled = !this.checked;
  };
  document.getElementById("fb-check").onchange = function () {
    document.getElementById("fb").disabled = !this.checked;
  };
  document.getElementById("yt-check").onchange = function () {
    document.getElementById("yt").disabled = !this.checked;
  };
  document.getElementById("tw-check").onchange = function () {
    document.getElementById("tw").disabled = !this.checked;
  };
</script>
@endsection
