@extends('layouts.app')
@section('content')
@include('layouts.header')

  <div class="nk-block">
    <div class="nk-tb-list is-separate mb-3">
      <div class="nk-tb-item nk-tb-head">
        <div class="nk-tb-col"><span class="sub-text">Nama Author</span></div>
        <div class="nk-tb-col"><span class="sub-text">Role</span></div>
        <div class="nk-tb-col"><span class="sub-text">Nama Akun</span></div>
        <div class="nk-tb-col text-center"><span class="sub-text">Instagram</span></div>
        <div class="nk-tb-col text-center"><span class="sub-text">Twitter</span></div>
        <div class="nk-tb-col text-center"><span class="sub-text">Facebook</span></div>
        <div class="nk-tb-col text-center"><span class="sub-text">Youtube</span></div>
        <div class="nk-tb-col">&nbsp;</div>
      </div>
      @foreach ($accounts as $_accounts)
        <div class="nk-tb-item _list_{{ $_accounts->uid }}">
          <div class="nk-tb-col">
            <span class="tb-amount">{{$_accounts->user->name}}</span>
          </div>
          <div class="nk-tb-col">
            <span class="tb-amount">{{ $_accounts->user->role->role_name }}</span>
          </div>
          <div class="nk-tb-col">
            <span>{{$_accounts->name ? $_accounts->name : ''}}</span>
          </div>
          <div class="nk-tb-col text-center">
            @if ($_accounts->account_ig ) <em class="icon ni ni-check-circle-fill text-success title h4"></em> @else <em class="icon ni ni-cross-circle-fill text-danger title h4"></em> @endif
          </div>
          <div class="nk-tb-col text-center">
            @if ($_accounts->account_twt ) <em class="icon ni ni-check-circle-fill text-success title h4"></em> @else <em class="icon ni ni-cross-circle-fill text-danger title h4"></em> @endif
          </div>
          <div class="nk-tb-col text-center">
            @if ($_accounts->account_fb ) <em class="icon ni ni-check-circle-fill text-success title h4"></em> @else <em class="icon ni ni-cross-circle-fill text-danger title h4"></em> @endif
          </div>
          <div class="nk-tb-col text-center">
            @if ($_accounts->account_yt ) <em class="icon ni ni-check-circle-fill text-success title h4"></em> @else <em class="icon ni ni-cross-circle-fill text-danger title h4"></em> @endif
          </div>
          <div class="nk-tb-col nk-tb-col-tools">
            <div class="dropdown">
              <a class="text-soft dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown" data-offset="-8,0"><em class="icon ni ni-more-h"></em></a>
              <div class="dropdown-menu dropdown-menu-right dropdown-menu-xs">
                <ul class="link-list-plain">
                  <li><a href="{{ url('/account/edit/'.$_accounts->uid) }}" class="text-primary">Ubah Akun</a></li>
                  <li><a href="javascript:;" attr="{{ $_accounts->uid }}" class="remove_sosmed text-danger">Hapus Akun</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
    <div class="card">
      <div class="card-inner">
        {{ $accounts->links() }}
      </div>
    </div>
  </div>

@endsection


@section('script')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
    $(document).ready( function () {
      $('.remove_sosmed').on("click", function (e) {      
        var attr = $(this).attr('attr');
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!'
        }).then(function (result) {
          if (result.value) {
            $.ajax({
              method: 'GET',
              url: `${window.location.origin}/account/remove/account`,
              dataType: 'json',
              data: { 'id': attr },
              success: function(result){
                $('._list_'+attr).remove();
                return Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
              }
            });
          }
        });
      });
    });
  </script>
@endsection
