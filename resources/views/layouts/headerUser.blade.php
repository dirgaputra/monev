@if (session('status'))
  <div class="alert alert-success" role="alert">{{ session('status') }}</div>
@endif
@if (session('error'))
  <div class="alert alert-danger" role="alert">{{ session('error') }}</div>
@endif
<div class="nk-block-head nk-block-head-sm">
  <div class="nk-block-between">
    <div class="nk-block-head-content">
      <h3 class="nk-block-title page-title">{{ $title }}</h3>
      <div class="nk-block-des text-soft">
        <p>Kamu punya <b>{{$total}}</b> data.</p>
      </div>
    </div>
    <div class="nk-block-head-content">
      <div class="toggle-wrap nk-block-tools-toggle">
        <div class="toggle-expand-content" data-content="more-options">
          <form action="{{ url()->current() }}" method="get">
            <ul class="nk-block-tools g-3">
              <li>
                <div class="form-control-wrap">
                  <div class="form-icon form-icon-right">
                    <em class="icon ni ni-search"></em>
                  </div>
                  <input type="text" class="form-control" id="default-04" name="search" value="{{ isset($_GET['search']) ? $_GET['search'] : "" }}" placeholder="Search by name">
                </div>
              </li>
              <li class="nk-block-tools-opt">
                <button class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-search"></em><span>Cari</span></button>
              </li>
            </ul>
          </form>
        </div>

      </div>
    </div>
  </div>
</div>
@if(Auth::user()->role_id === 1)
  <div class="nk-block-head nk-block-head-sm">
    <a href="{{ route('create_user') }}" class="btn btn-xl btn-outline-dark w-100 text-center">
      <span>TAMBAH USER</span>
    </a>
  </div>
@endif
