<div class="nk-sidebar-element">
  <div class="nk-sidebar-content">
    <div class="nk-sidebar-menu" data-simplebar>
      <ul class="nk-menu">
        <li class="nk-menu-item">
          <a href="{{ route('dashboard') }}" class="nk-menu-link">
            <span class="nk-menu-icon"><em class="icon ni ni-chart-down"></em></span>
            <span class="nk-menu-text">Dashboard</span>
          </a>
        </li>
        
        <li class="nk-menu-item has-sub">
          <a href="#" class="nk-menu-link nk-menu-toggle">
            <span class="nk-menu-icon"><em class="icon ni ni-activity-alt"></em></span>
            <span class="nk-menu-text">Kegiatan Dikmas</span>
          </a>
          <ul class="nk-menu-sub">
            <li class="nk-menu-item">
              <a href="{{ route('activityAll') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-chevron-right-circle"></em></span>
                <span class="nk-menu-text">Lihat Semua</span>
              </a>
            </li>
            
            <li class="nk-menu-item">
              <a href="{{ url('/activity/cat/kampung-tertib-lalu-lintas') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-chevron-right-circle"></em></span>
                <span class="nk-menu-text">Kampung Tertib Lalu Lintas</span>
              </a>
            </li>
            
            <li class="nk-menu-item">
              <a href="{{ url('/activity/cat/smart-city') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-chevron-right-circle"></em></span>
                <span class="nk-menu-text">Smart City</span>
              </a>
            </li>
            
            <li class="nk-menu-item">
              <a href="{{ url('/activity/cat/taman-lantas') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-chevron-right-circle"></em></span>
                <span class="nk-menu-text">Taman Lantas</span>
              </a>
            </li>
            <li class="nk-menu-item">
              <a href="{{ url('/activity/cat/pks') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-chevron-right-circle"></em></span>
                <span class="nk-menu-text">PKS</span>
              </a>
            </li>
            <li class="nk-menu-item">
              <a href="{{ url('/activity/cat/cara-aman-ke-sekolah') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-chevron-right-circle"></em></span>
                <span class="nk-menu-text">Cara Aman ke Sekolah</span>
              </a>
            </li>
            <li class="nk-menu-item">
              <a href="{{ url('/activity/cat/masdarwis') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-chevron-right-circle"></em></span>
                <span class="nk-menu-text">Masdarwis</span>
              </a>
            </li>
            <li class="nk-menu-item">
              <a href="{{ url('/activity/cat/pocil') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-chevron-right-circle"></em></span>
                <span class="nk-menu-text">Pocil</span>
              </a>
            </li>
            <li class="nk-menu-item">
              <a href="{{ url('/activity/cat/kampus-pelopor-tertib-berlalu-lintas') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-chevron-right-circle"></em></span>
                <span class="nk-menu-text">Kampus Pelopor Tertib Berlalu Lintas</span>
              </a>
            </li>
            <li class="nk-menu-item">
              <a href="{{ url('/activity/cat/pembinaan-komunitas') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-chevron-right-circle"></em></span>
                <span class="nk-menu-text">Pembinaan Komunitas</span>
              </a>
            </li>
            <li class="nk-menu-item">
              <a href="{{ url('/activity/cat/pameran') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-chevron-right-circle"></em></span>
                <span class="nk-menu-text">Pameran</span>
              </a>
            </li>
            <li class="nk-menu-item">
              <a href="{{ url('/activity/cat/manajemen-media') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-chevron-right-circle"></em></span>
                <span class="nk-menu-text">Manajemen Media</span>
              </a>
            </li>
            <li class="nk-menu-item">
              <a href="{{ url('/activity/cat/7-program-prioritas') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-chevron-right-circle"></em></span>
                <span class="nk-menu-text">7 Program Prioritas</span>
              </a>
            </li>
            <li class="nk-menu-item">
              <a href="{{ url('/activity/cat/indonesia-layak-anak-idola') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-chevron-right-circle"></em></span>
                <span class="nk-menu-text">Indonesia Layak Anak (Idola)</span>
              </a>
            </li>
          </ul>
          <!-- .nk-menu-sub -->
        </li>

        <li class="nk-menu-item has-sub">
          <a href="#" class="nk-menu-link nk-menu-toggle">
            <span class="nk-menu-icon"><em class="icon ni ni-users"></em></span>
            <span class="nk-menu-text">Sosial Media</span>
          </a>
          <ul class="nk-menu-sub">
            <li class="nk-menu-item">
              <a href="{{ route('account') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-user-fill"></em></span>
                <span class="nk-menu-text">Semua Akun</span>
              </a>
            </li>
            <li class="nk-menu-item">
              <a href="{{ route('facebook') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-facebook-fill"></em></span>
                <span class="nk-menu-text">Facebook</span>
              </a>
            </li>
            <li class="nk-menu-item">
              <a href="{{ route('instagram') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-instagram"></em></span>
                <span class="nk-menu-text">Instagram</span>
              </a>
            </li>
            <li class="nk-menu-item">
              <a href="{{ route('twitter') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-twitter"></em></span>
                <span class="nk-menu-text">Twitter</span>
              </a>
            </li>
            <li class="nk-menu-item">
              <a href="{{ route('youtube') }}" class="nk-menu-link">
                <span class="nk-menu-icon"><em class="icon ni ni-youtube-fill"></em></span>
                <span class="nk-menu-text">YouTube</span>
              </a>
            </li>
          </ul>
          <!-- .nk-menu-sub -->
        </li>
        
        <li class="nk-menu-item">
          <a href="{{ route('user') }}" class="nk-menu-link">
            <span class="nk-menu-icon"><em class="icon ni ni-user-alt-fill"></em></span>
            <span class="nk-menu-text">User</span>
          </a>
        </li>
        @if (Auth::user()->role_id === 1 || Auth::user()->role_id === 2)
          <li class="nk-menu-item">
            <a href="{{ route('videoAll') }}" class="nk-menu-link">
              <span class="nk-menu-icon"><em class="icon ni ni-video"></em></span>
              <span class="nk-menu-text">Video</span>
            </a>
          </li>
        @endif
        
        @if (Auth::user()->role_id === 1 || Auth::user()->role_id === 2)
          <li class="nk-menu-item">
            <a href="{{ route('sliderAll') }}" class="nk-menu-link">
              <span class="nk-menu-icon"><em class="icon ni ni-img"></em></span>
              <span class="nk-menu-text">Slider</span>
            </a>
          </li>
        @endif

        <!-- .nk-menu-item -->
        <li class="nk-menu-item">
          <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nk-menu-link">
            <span class="nk-menu-icon"><em class="icon ni ni-signout"></em></span>
            <span class="nk-menu-text">Keluar</span>
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
          </form>
        </li>
        <!-- .nk-menu-item -->
       
      </ul>
      <!-- .nk-menu -->
    </div>
    <!-- .nk-sidebar-menu -->
  </div>
  <!-- .nk-sidebar-content -->
</div>