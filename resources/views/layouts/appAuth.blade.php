<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Monev</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha512-s+xg36jbIujB2S2VKfpGmlC3T5V2TF3lY48DX7u2r9XzGzgPsa6wTpOQA7J9iffvdeBN0q9tKzRxVxw1JviZPg==" crossorigin="anonymous"></script>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link rel="shortcut icon" href="{{ asset('/images/korlantas.png') }}" type="image/x-icon"  />
    <link href="https://fonts.googleapis.com/css2?family=Kumbh+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css" integrity="sha512-/zs32ZEJh+/EO2N1b0PEdoA10JkdC3zJ8L5FTiQu82LR9S/rOQNfQN7U59U9BC12swNeRAz3HSzIL2vpp4fv3w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{ asset('css/dashlite.css?ver=1.9.2') }}">
    <link id="skin-default" rel="stylesheet" href="{{ asset('css/theme.css?ver=1.9.2') }}">
  </head>
  <body class="nk-body bg-white npc-default pg-auth">
    <div class="nk-app-root">
      <div class="nk-main ">
        <div class="nk-wrap nk-wrap-nosidebar">
          <div class="nk-content ">
            @yield('content')
          </div>
        </div>
      </div>
    </div>
    @yield('script')
    <script src="{{ asset('js/bundle.js?ver=1.9.2') }}"></script>
    <script src="{{ asset('js/scripts.js?ver=1.9.2') }}"></script>
    <script src="{{ asset('js/charts/gd-default.js?ver=1.9.2') }}"></script>
  </body>
</html>
