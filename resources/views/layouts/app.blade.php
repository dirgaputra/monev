<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <base href="../" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" href="{{ asset('/images/korlantas.png') }}" type="image/x-icon"  />
    <title>Monev - {{$title}}</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha512-s+xg36jbIujB2S2VKfpGmlC3T5V2TF3lY48DX7u2r9XzGzgPsa6wTpOQA7J9iffvdeBN0q9tKzRxVxw1JviZPg==" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css" integrity="sha512-/zs32ZEJh+/EO2N1b0PEdoA10JkdC3zJ8L5FTiQu82LR9S/rOQNfQN7U59U9BC12swNeRAz3HSzIL2vpp4fv3w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{ asset('css/dashlite.css?ver=1.9.2') }}" />
    <link id="skin-default" rel="stylesheet" href="{{ asset('css/theme.css?ver=1.9.2') }}" />
  </head>
  <body class="nk-body bg-lighter npc-general has-sidebar">
    <div class="nk-app-root">
      <div class="nk-main">
        <div class="nk-sidebar nk-sidebar-fixed is-light" data-content="sidebarMenu">
          <div class="nk-sidebar-element nk-sidebar-head">
            <div class="nk-sidebar-brand">
              <a href="{{ route('dashboard') }}" class="logo-link nk-sidebar-logo">
                <img class="logo-light logo-img" src="{{ asset('/images/korlantas.png') }}" alt="logo" />
                <img class="logo-dark logo-img" src="{{ asset('/images/korlantas.png') }}" alt="logo-dark" />
                <span style="font-size: 28px;" class="pl-3"><strong>Monev</strong></span>
              </a>
            </div>
            <div class="nk-menu-trigger mr-n2">
              <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
            </div>
          </div>
          
          @include('layouts.sidebar')
          
          <!-- .nk-sidebar-element -->
        </div>
        <!-- sidebar @e -->
        <!-- wrap @s -->
        <div class="nk-wrap">
          <!-- main header @s -->
          <div class="nk-header nk-header-fixed is-light">
            <div class="container-fluid">
              <div class="nk-header-wrap">
                <div class="nk-menu-trigger d-xl-none ml-n1">
                  <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                </div>
                <div class="nk-header-brand d-xl-none">
                  <a href="html/index.html" class="logo-link">
                    <img class="logo-light logo-img" src="{{ asset('/images/korlantas.png') }}" alt="logo" />
                    <img class="logo-dark logo-img" src="{{ asset('/images/korlantas.png') }}" alt="logo-dark" />
                    MONEV
                  </a>
                </div>
                <!-- .nk-header-brand -->
                {{--
                <div class="nk-header-news d-none d-xl-block">
                  <div class="nk-news-list">
                    <a class="nk-news-item" href="#">
                      <div class="nk-news-icon">
                        <em class="icon ni ni-card-view"></em>
                      </div>
                      <div class="nk-news-text">
                        <p>Do you know the latest update of 2019? <span> A overview of our is now available on YouTube</span></p>
                        <em class="icon ni ni-external"></em>
                      </div>
                    </a>
                  </div>
                </div>
                <!-- .nk-header-news -->
                --}}
                <div class="nk-header-tools">
                  <ul class="nk-quick-nav">
                    <li class="dropdown user-dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <div class="user-toggle">
                          <div class="user-avatar sm">
                            <em class="icon ni ni-user-alt"></em>
                          </div>
                          <div class="user-info d-none d-md-block">
                            {{--
                            <div class="user-status">{{ Auth::user()->role_id }}</div>
                            --}}
                            <div class="user-name dropdown-indicator">{{ Auth::user()->name }}</div>
                          </div>
                        </div>
                      </a>
                      <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                        <div class="dropdown-inner">
                          <ul class="link-list">
                            <li>
                              <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <em class="icon ni ni-signout"></em><span>Keluar</span>
                              </a>
                              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                              </form>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </li>
                  </ul>
                  <!-- .nk-quick-nav -->
                </div>
                <!-- .nk-header-tools -->
              </div>
              <!-- .nk-header-wrap -->
            </div>
            <!-- .container-fliud -->
          </div>
          <!-- main header @e -->
          <!-- content @s -->
          <div class="nk-content">
            <div class="container-fluid">
              <div class="nk-content-inner">
                <div class="nk-content-body">
                  @yield('content')
                </div>
              </div>
            </div>
          </div>
          <!-- content @e -->
          <!-- footer @s -->
          <div class="nk-footer">
            <div class="container-fluid">
              <div class="nk-footer-wrap">
                <div class="nk-footer-copyright">&copy; 2020 Korlantas. All rights reserved.</div>
                {{--
                <div class="nk-footer-links">
                  <ul class="nav nav-sm">
                    <li class="nav-item"><a class="nav-link" href="#">Terms</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Privacy</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Help</a></li>
                  </ul>
                </div>
                --}}
              </div>
            </div>
          </div>
          <!-- footer @e -->
        </div>
        <!-- wrap @e -->
      </div>
      <!-- main @e -->
    </div>
    @yield('script')
    <script src="{{ asset('js/bundle.js?ver=1.9.2') }}"></script>
    <script src="{{ asset('js/scripts.js?ver=1.9.2') }}"></script>
    <script src="{{asset('js/example-sweetalert.js?ver=1.9.2')}}"></script>
    {{-- <script src="{{ asset('js/charts/chart-ecommerce.js?ver=1.9.2') }}"></script> --}}
    {{-- <script src="{{ asset('js/charts/gd-default.js?ver=1.9.2') }}"></script> --}}
   
  </body>
</html>
