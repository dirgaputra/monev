@extends('layouts.app')

@section('content')
    @if (session('status'))
      <div class="alert alert-success" role="alert">{{ session('status') }}</div>
    @endif
    <div class="nk-block-head nk-block-head-sm">
      <div class="nk-block-between">
        <div class="nk-block-head-content">
          <h3 class="nk-block-title page-title">{{ $title }} ({{$total}})</h3>
        </div>
        <div class="nk-block-head-content">
          <div class="toggle-wrap nk-block-tools-toggle">
            <a href="{{ route('create_account') }}"><button class="btn btn-primary">Tambah Akun</button></a>
          </div>
        </div>
      </div>
    </div>

    <div class="nk-block">
      <div class="nk-tb-list is-separate mb-3">
        <div class="nk-tb-item nk-tb-head">
          <div class="nk-tb-col"><span class="sub-text">Nama</span></div>
          <div class="nk-tb-col"><span class="sub-text">Provinsi</span></div>
          <div class="nk-tb-col"><span class="sub-text">Nama Akun</span></div>
          <div class="nk-tb-col text-center"><span class="sub-text">Kiriman</span></div>
          <div class="nk-tb-col text-center"><span class="sub-text">Langganan</span></div>
          <div class="nk-tb-col">&nbsp;</div>
        </div>
        @foreach ($accounts as $_accounts)
          <div class="nk-tb-item">
              <div class="nk-tb-col">
                <span class="tb-amount">{{$_accounts->name}}</span>
              </div>
              <div class="nk-tb-col">
                <span class="tb-amount">{{$_accounts->prov_id}}</span>
              </div>
              <div class="nk-tb-col">
                <span>{{$_accounts->yt_id}}</span>
              </div>
              <div class="nk-tb-col text-center ig_post_{{$_accounts->yt_id}}">
                <div class="spinner-border spinner-border-sm" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>
              <div class="nk-tb-col text-center ig_flwrs_{{$_accounts->yt_id}}">
                <div class="spinner-border spinner-border-sm" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>
              <div class="nk-tb-col nk-tb-col-tools">
                <div class="dropdown">
                  <a class="text-soft dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown" data-offset="-8,0"><em class="icon ni ni-more-h"></em></a>
                  <div class="dropdown-menu dropdown-menu-right dropdown-menu-xs">
                    <ul class="link-list-plain">
                      <li><a href="edit/youtube/{{$_accounts->id}}" class="text-primary">Edit</a></li>
                      <li><a href="https://www.youtube.com/channel/{{$_accounts->yt_id}}" target="_blank"" class="text-primary">View</a></li>
                      {{-- <li><a href="#" class="text-danger">Remove</a></li> --}}
                    </ul>
                  </div>
                </div>
              </div>
          </div>
        @endforeach
      </div>

      <div class="card">
        <div class="card-inner">
          {{ $accounts->links() }}
        </div>
      </div>
    </div>
        
@endsection

@section('script')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
    $(document).ready( function () {
      @foreach ($accounts as $_accounts)
        $.ajax({
          method: 'GET',
          url: "https://monev.rasirosakorlantas.id/getYT",
          // url: "{{ url('getYT') }}", 
          dataType: 'json',
          data: { id: "{{$_accounts->yt_id}}"},
          success: function(result){
            $('.ig_post_'+"{{$_accounts->yt_id}}").html(result.post)
            $('.ig_flwrs_'+"{{$_accounts->yt_id}}").html(result.subs)
          }
        });
      @endforeach
  });
  </script>

@endsection
