<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $fillable = [
        'uid', 'user_id', 'title', 'fileName', 'path', 'created_at'
    ];

    public function user(){
    	return $this->hasOne(User::class, 'uuid', 'user_id');
    }

    protected $table = 'video';
}
