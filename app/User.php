<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'id', 'uuid', 'name', 'email', 'password', 'role_id', 'polri_id', 'polda_id', 'polres_id',
    ];

    public function role(){
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    public function polri(){
    	return $this->hasOne(Polri::class, 'id', 'polri_id');
    }
    
    public function polda(){
    	return $this->hasOne(Polda::class, 'id', 'polda_id');
    }

    public function polres(){
    	return $this->hasOne(Polres::class, 'id', 'polres_id');
    }

    public function account(){
    	return $this->belongsTo(Account::class);
    }

    public function slider(){
    	return $this->belongsTo(Slider::class, 'author', 'uuid');
    }
    public function activity(){
    	return $this->hasMany(Activity::class, 'user_id', 'uuid');
    }

    protected $hidden = [
        'password', 'remember_token',
    ];
}
