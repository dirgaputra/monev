<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Role extends Authenticatable
{
    use Notifiable;
    protected $fillable = [
        'id', 'name', 
    ];

    public function user(){
    	return $this->belongsTo(User::class);
    }
}
