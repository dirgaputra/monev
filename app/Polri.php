<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Polri extends Model
{
    
    public function user(){
    	return $this->belongsTo(User::class);
    }
    
    protected $fillable = [
        'id', 'name_polri',
    ];

    protected $table = 'polri';
}
