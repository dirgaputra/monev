<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Polres extends Model
{
    public function user(){
    	return $this->belongsTo(User::class);
    }
    
    protected $table = 'polres';
}
