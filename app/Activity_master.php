<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity_master extends Model
{
    protected $fillable = [
        'id', 'name', 'slug'
    ];

    public function activity(){
    	// return $this->belongsTo(Activity::class, 'activity_id', 'id');
    	return $this->belongsTo(Activity::class, 'id', 'activity_id');
    }

    protected $table = 'activity_master';
}
