<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account_twt extends Model
{
    protected $fillable = [
        'id',  'user_account', 'account', 'post', 'followers', 'following', 'created_at', 'updated_at'
    ];

    public function accounts(){
    	return $this->belongsTo(Account::class, 'user_account', 'uid');
    }


    public $incrementing = false;
}
