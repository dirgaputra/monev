<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{

    protected $fillable = [
        'uid', 'user_id', 'name'
    ];

    public function user(){
    	return $this->hasOne(User::class, 'uuid' , 'user_id');
    }

    public function account_twt(){
    	return $this->hasOne(Account_twt::class, 'user_account', 'uid' )->orderBy('post');
    }

    public function account_yt(){
    	return $this->hasOne(Account_yt::class, 'user_account', 'uid' )->orderBy('post');
    }
    
    public function account_fb(){
    	return $this->hasOne(Account_fb::class, 'user_account', 'uid' )->orderBy('post');
    }

    public function account_ig(){
    	return $this->hasOne(Account_ig::class, 'user_account', 'uid' )->orderBy('post');
    }

    public $incrementing = false;
}
