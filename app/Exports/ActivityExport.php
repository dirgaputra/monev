<?php

namespace App\Exports;

use App\Activity_master;
use App\Activity;
use App\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ActivityExport implements FromView{
    protected $slug;

    function __construct($slug) {
        $this->slug = $slug;
    }

    public function view(): View{
        $query = Activity::whereHas('user', function($query){
            Auth::user()->role_id !== 1 && $query->where('role_id', Auth::user()->role_id);
        })->orderBy('created_at', 'desc');
        
        if($this->slug !== 'all'){
            $getId = Activity_master::where('activity_master.slug', $this->slug)
            ->first();
            $query->where('activity_id', $getId->id);
        }

        $data['report'] = $query->get();

        $data['title'] = str_replace('-', ' ', $this->slug);
        $data['date'] = date("Y-m-d h:i:s");

        return view('activity.reportActivity', $data);
    }
}
