<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Redirect;
use Psr\SimpleCache\CacheInterface;
use Instagram\Api;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Alaouy\Youtube\Facades\Youtube;
use App\Account;
use App\Account_twt;
use App\Account_yt;
use App\Account_fb;
use App\Account_ig;
use App\Province;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Datatables;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use \Twitter;

class FetchSosmedController extends Controller{
  
  public function __construct(){ 
    $this->middleware('auth');
  }  

  public function getTWT(Request $request){
    $data = $request->all();
    $res = array();
    try {
      $channel = Twitter::getUsers(['screen_name' => $data['account']]);
      Account_twt::where('account', $data['account'])
      ->update([
        'post' => $channel->statuses_count,
        'followers' => $channel->followers_count,
        'following' => $channel->friends_count
      ]);

      $res = array (
        "account" => str_replace('.', '_', $data['account']),
        "post" => number_format($channel->statuses_count),
        "followers" => number_format($channel->followers_count),
        "following" => number_format($channel->friends_count),
      );
    } catch (\Throwable $th) {
      $res = array (
        "account" => str_replace('.', '_', $data['account']),
        "post" =>"<span class='text-danger'>Not Found</span>",
        "followers" => "<span class='text-danger'>Not Found</span>",
        "following" => "<span class='text-danger'>Not Found</span>",
      );
    }
    return $res;
  }


  public function getYT(Request $request){
    $data = $request->all();
    $res = array();
    try {
      $channel = Youtube::getChannelById($data['account']);
      Account_yt::where('account', $data['account'])
      ->update([
        'post' => $channel->statistics->videoCount,
        'followers' => $channel->statistics->subscriberCount,
        'following' => $channel->statistics->subscriberCount
      ]);
      $res = array (
        "account" => str_replace('.', '_', $data['account']),
        "post" => number_format($channel->statistics->videoCount),
        "subs" => number_format($channel->statistics->subscriberCount),
      );
    } catch (\Throwable $th) {
      $res = array (
        "account" => str_replace('.', '_', $data['account']),
        "post" => "<span class='text-danger'>Not Found</span>",
        "subs" => "<span class='text-danger'>Not Found</span>",
      );
    }
    return $res;
  }


  public function getIG(Request $request){
    $data = $request->all();
    $res = array();
    try {
      $cachePool = new FilesystemAdapter('Instagrams', 0, __DIR__ . '/../cache');
      $api = new Api($cachePool);
      $api->login('dirgaputraa', 'vanmarwijk1892');
      $account = $api->getProfile($data['account']);
      Account_ig::where('account', $data['account'])
      ->update([
        'post' => $account->getMediaCount(),
        'followers' => $account->getFollowers(),
        'following' => $account->getFollowing()
      ]);
      $res = array (
        "account" => str_replace('.', '_', $data['account']),
        "post" => number_format($account->getMediaCount()),
        "followers" => number_format($account->getFollowers()),
        "following" => number_format($account->getFollowing()),
      );
    } catch (\Throwable $th) {
      $res = array (
        "account" => str_replace('.', '_', $data['account']),
        "post" => "<span class='text-danger'>Not Found</span>",
        "followers" => "<span class='text-danger'>Not Found</span>",
        "following" => "<span class='text-danger'>Not Found</span>",
      );
    }
    return $res;
  }

}
