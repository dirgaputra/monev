<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use App\Activity_master;
use App\Activity;
use App\User;
use App\Video;
use Redirect;

class VideoController extends Controller{
  public function __construct(){ 
    $this->middleware('auth');
  }
    
  public function index(Request $request){
    $data['title'] = 'Video Master';
    $data['query'] = Video::orderBy('created_at', 'desc');
    $data['total'] = $data['query']->count();
    $data['video'] = $data['query']->paginate(10);
    $data['video']->appends($request->only('search'));
    return view('video.video', $data);
  }
  
  public function create(Request $request){
    $data['title'] = 'Tambah Video';
    return view('video.createVideo', $data);
  }
  
  public function store(Request $request){
    $auth = Auth::user();
    $request->validate([
      'file' => 'required',
      'title' => 'required',
    ]);;
    if($request->file){
      $fileName = time().'_'.$request->file->getClientOriginalName();
      $filePath = $request->file('file')->move('uploads/videos', $fileName);
      // $pathInfo = pathinfo($fileName);
      // $pathInfo['extension'];
      Video::create([
        'uid' =>  Str::uuid(),
        "user_id" => $auth->uuid,
        "title" => $request->title,
        "fileName" =>  $fileName,
        "path" => $filePath,
        "created_at" => date('Y-m-d H:i:s')
      ]);
      return redirect('/video/create')->with('status', 'Data berhasil di masukan!'); 
    }
  
  }

  public function edit(Request $request,  $id){
    $data['title'] = 'Tambah Aktifitas';
    $data['video'] = Video::where('uid', $id)->first();
    return view('video.editVideo', $data);
  }

  public function editStore(Request $request, $id){
    $auth = Auth::user();
    $query = Video::where('uid', $id);
    $request->validate([ 'title' => 'required', ]);

    if($request->file){
      $fileName = time().'_'.$request->file->getClientOriginalName();
      $filePath = $request->file('file')->move('uploads/videos', $fileName);
      if(File::exists($query->first()->path)) {
        File::delete($query->first()->path);
      }
    }
    $query->update([
      "title" => $request->title,
      "fileName" => $request->file ? $fileName : $query->first()->fileName,
      "path" => $request->file ? $filePath : $query->first()->path,
    ]);
    return redirect('/video/edit/'.$id)->with('status', 'Data berhasil di masukan!'); 
   }

   public function delete(Request $request){ 
    $data = $request->all();
    $query = Video::where('uid', $data['id']);
    if(File::exists($query->first()->path)) {
      File::delete($query->first()->path);
    }
    $query->delete();
    $status = array(
      "status" => 200,
      "id" => $data['id'],
    );
    return $status;
  }

}
