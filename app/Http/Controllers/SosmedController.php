<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Redirect;
use Psr\SimpleCache\CacheInterface;
use Instagram\Api;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Alaouy\Youtube\Facades\Youtube;
use App\Account;
use App\Account_twt;
use App\Account_ig;
use App\Account_yt;
use App\Account_fb;
use App\Province;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use \Twitter;
use \Facebook\Facebook;
use Laravel\Socialite\Facades\Socialite;

// use \Facebook\FacebookRedirectLoginHelper;

class SosmedController extends Controller{
   
  public function __construct(){ 
    $this->middleware('auth');
  }

  public function account(Request $request){
    $data['title'] = 'Semua Akun';
    $data['query'] = Account::
    where(function($query) use($request){
      $query->where('name', 'like', "%{$request->search}%");
      $query->whereHas('user', function($query) use($request){
        $query->where('name', 'like', "%{$request->search}%")
        ->orWhereHas('role', function($query) use($request){
          $query->where('role_name', 'like', "%{$request->search}%");
        });
      });
    })
    ->orderBy('created_at', 'desc');
    Auth::user()->role_id !== 1 && $data['query']->where('user_id', Auth::user()->uuid);
    $data['total'] = $data['query']->count();
    $data['accounts'] = $data['query']->paginate(10);
    $data['accounts']->appends($request->only('search'));
    return view('accounts.account', $data);
  }

  public function instagram(Request $request){
    $data['title'] = 'Instagram';
    $data['query'] = Account_ig::has('accounts')
    ->whereHas('accounts', function($query) use($request){
      Auth::user()->role_id !== 1 && $query->where('user_id', Auth::user()->uuid);
      $query->where(function($q) use($request){
        $q->where('name', 'like', "%{$request->search}%")
        ->orWhereHas('user', function($query) use($request){
          $query->where('name', 'like', "%{$request->search}%")
          ->orWhereHas('role', function($query) use($request){
            $query->where('role_name', 'like', "%{$request->search}%");
          });
        });
      });
    })
    ->where(function($query) use($request){
      $query->orWhere('account', 'like', "%{$request->search}%");
    })
    ->orderBy('created_at', 'desc');
    $data['total'] = $data['query']->count();
    $data['accounts'] = $data['query']->paginate(10);
    $data['accounts']->appends($request->only('search'));
    return view('accounts.instagram', $data);
  }

  public function youtube(Request $request){
    $data['title'] = 'YouTube';
    $data['query'] = Account_yt::has('accounts')
    ->whereHas('accounts', function($query) use($request){
      Auth::user()->role_id !== 1 && $query->where('user_id', Auth::user()->uuid);
      $query->where(function($q) use($request){
        $q->where('name', 'like', "%{$request->search}%")
        ->orWhereHas('user', function($query) use($request){
          $query->where('name', 'like', "%{$request->search}%")
          ->orWhereHas('role', function($query) use($request){
            $query->where('role_name', 'like', "%{$request->search}%");
          });
        });
      });
    })
    ->where(function($query) use($request){
      $query->orWhere('account', 'like', "%{$request->search}%");
    })
    ->orderBy('created_at', 'desc')
    ; // doesntHave;
    $data['total'] = $data['query']->count();
    $data['accounts'] = $data['query']->paginate(10);
    $data['accounts']->appends($request->only('search'));
    return view('accounts.youtube', $data);
  }

  public function handleProviderCallback(Request $request){
    $user = Socialite::driver('facebook')->user();
    dd($user->token);

    // return Socialite::driver('facebook')->redirect();
  }

  public function facebooks(Request $request){
    $fb = new Facebook([
      'app_id' => '820341895439825',
      'app_secret' => 'e0c7d1b1a11f43cf63718c21594d63cc',
      // 'default_graph_version' => 'v2.10',
      'default_graph_version' => 'v2.4',
      'default_access_token' => 'EAALqGLtFpdEBABPQymx6wNDq0mWT5SGtWQJggjwwnNs4pW2Nr5GOsQXP9LbZB0EBgcei2ISmpBtGvHTRsovlvZBzdKrSp2lDleI9p00tVjlyvkGXm40W1QZCyjJwx6llZCLEz7SUfi4EP7ZAnxjKGcZBGZAy9JYyoUZD', 
    ]);

    // try {
      // $response = $fb->get('/me', 'EAALqGLtFpdEBABPQymx6wNDq0mWT5SGtWQJggjwwnNs4pW2Nr5GOsQXP9LbZB0EBgcei2ISmpBtGvHTRsovlvZBzdKrSp2lDleI9p00tVjlyvkGXm40W1QZCyjJwx6llZCLEz7SUfi4EP7ZAnxjKGcZBGZAy9JYyoUZD');
      $response = $fb->get('/page-id/feed', 'EAALqGLtFpdEBABPQymx6wNDq0mWT5SGtWQJggjwwnNs4pW2Nr5GOsQXP9LbZB0EBgcei2ISmpBtGvHTRsovlvZBzdKrSp2lDleI9p00tVjlyvkGXm40W1QZCyjJwx6llZCLEz7SUfi4EP7ZAnxjKGcZBGZAy9JYyoUZD');
      dd($response);
    // } catch (\Throwable $th) {
      //throw $th;
    // }
  }
  public function facebook(Request $request){
    $fb = new Facebook([
      'app_id' => '820341895439825',
      'app_secret' => 'e0c7d1b1a11f43cf63718c21594d63cc',
      'default_graph_version' => 'v2.10',
      'default_access_token' => 'EAALqGLtFpdEBAKA3WA8Ojwvvdzi79kYGyCc0WcAy1iFel9FfsjnXNZBwnXuIz8H52QpuoTHFndZClGUOLO90JY6ZAaHaHVOPX1xrKh0x6qXZCcapDoRlR0EMvl6NMg5ohfvOGoNGlGjeMpRKZBhtvcoG8uOU24nhSx9otCxlhogZDZD', 
    ]);
    // $user = Socialite::driver('facebook')->redirect();
    //  $fb->getRedirectLoginHelper();
    // $session = $helper->getSessionFromRedirect();
    // $response = $fb->get('/adi.dakkaviper');
    // dd($response);
    
    $data['title'] = 'Facebook';
    $data['query'] = Account_fb::has('accounts')
    ->whereHas('accounts', function($query) use($request){
      Auth::user()->role_id !== 1 && $query->where('user_id', Auth::user()->uuid);
      $query->where(function($q) use($request){
        $q->where('name', 'like', "%{$request->search}%")
        ->orWhereHas('user', function($query) use($request){
          $query->where('name', 'like', "%{$request->search}%")
          ->orWhereHas('role', function($query) use($request){
            $query->where('role_name', 'like', "%{$request->search}%");
          });
        });
      });
    })
    ->where(function($query) use($request){
      $query->orWhere('account', 'like', "%{$request->search}%");
    })
    ->orderBy('created_at', 'desc');
    $data['total'] = $data['query']->count();
    $data['accounts'] = $data['query']->paginate(10);
    $data['accounts']->appends($request->only('search'));
    return view('accounts.facebook', $data);
  }

  public function twitter(Request $request){
    $data['title'] = 'Twitter';
    $data['query'] = Account_twt::has('accounts')
    ->whereHas('accounts', function($query) use($request){
      Auth::user()->role_id !== 1 && $query->where('user_id', Auth::user()->uuid);
      $query->where(function($q) use($request){
        $q->where('name', 'like', "%{$request->search}%")
        ->orWhereHas('user', function($query) use($request){
          $query->where('name', 'like', "%{$request->search}%")
          ->orWhereHas('role', function($query) use($request){
            $query->where('role_name', 'like', "%{$request->search}%");
          });
        });
      });
    })
    ->where(function($query) use($request){
      $query->orWhere('account', 'like', "%{$request->search}%");
    })
    ->orderBy('created_at', 'desc');
    $data['total'] = $data['query']->count();
    $data['accounts'] = $data['query']->paginate(10);
    $data['accounts']->appends($request->only('search'));
    return view('accounts.twitter', $data);
  }

  public function createAccount(){
    $data['provs'] = Province::query()->get();
    $data['title'] = 'Create Account';
    $data['users'] = User::all()->whereNotIn('role_id', 1);
    return view('accounts.createAccount', $data);
  }

  public function submitAccount(Request $request){
    try {
      $data = $request->all();
      $auth = Auth::user();
      $account = Account::create([
        'uid' =>  Str::uuid(),
        'user_id' =>  $auth->role_id === 1 ? $data['user_id'] : $auth->uuid,
        'name' =>  $data['name'] ,
      ]);
      try {
        if(isset($data['tw'])){
          $twt_ =  Twitter::getUsers(['screen_name' => $data['tw']]);
          Account_twt::create([
            'user_account' => $account->uid,
            'account' =>  $data['tw'] ,
            'post' => $twt_->statuses_count,
            'followers' => $twt_->followers_count,
            'following' => $twt_->friends_count,
          ]);
        }
      } catch (\Throwable $th) {
        return redirect('/account/create')->with('error', 'Twitter anda salah!');
      }

      try {
        if(isset($data['ig'])){
          $cachePool = new FilesystemAdapter('Instagrams', 0, __DIR__ . '/../cache');
          $api = new Api($cachePool);
          $api->login('dirgaputraa', 'vanmarwijk1892');
          $ig_ = $api->getProfile($data['ig']);
          Account_ig::create([
            'user_account' => $account->uid,
            'account' => $data['ig'],
            'post' => $ig_->getMediaCount(),
            'followers' => $ig_->getFollowers(),
            'following' => $ig_->getFollowing(),
          ]);
        }
      } catch (\Throwable $th) {
        return redirect('/account/create')->with('error', 'Instagram anda salah!');
      }

      try {
        if(isset($data['yt'])){
          $yt_ = Youtube::getChannelById($data['yt']);
          Account_yt::create([
            'user_account' => $account->uid,
            'account' => $data['yt'],
            'post' => $yt_->statistics->videoCount,
            'followers' => $yt_->statistics->subscriberCount,
            'following' => $yt_->statistics->subscriberCount,
          ]);
        }
      } catch (\Throwable $th) {
        return redirect('/account/create')->with('error', 'Youtube anda salah!');
      }

      try {
        if(isset($data['fb'])){
          Account_fb::create([
            'user_account' => $account->uid,
            'account' => $data['fb'],
            'post' => 0,
            'followers' => 0,
            'following' => 0,
          ]);
        }
      } catch (\Throwable $th) {
        return redirect('/account/create')->with('error', 'Facebook anda salah!');
      }

      return redirect('/account/create')->with('status', 'Data '.$data['name'].' berhasil di masukan!');

    } catch (\Throwable $th) {
      return redirect('/account/create')->with('error', 'Data gagal di masukan!');
    }
  }

  public function removeAccount(Request $request){ 
    $data = $request->all();
    $query = Account::where('uid', $data['id']);
    $query->first()->account_twt  && $query->first()->account_twt->delete();
    $query->first()->account_ig   && $query->first()->account_ig->delete();
    $query->first()->account_fb   && $query->first()->account_fb->delete();
    $query->first()->account_yt   && $query->first()->account_yt->delete();
    $query->delete();
    $status = array(
      "status" => 200,
      "id" => $data['id'],
    );
    return $status;
  }

  public function removeSosmed(Request $request){ 
    $data = $request->all();
    $query = DB::table($data['type'])->where('id', $data['id']);
    $query->delete();
    $status = array(
      "status" => 200,
      "id" => $data['id']
    );
    return $status;
  }
  
  public function editSosmed($id){ 
    $data['title'] = 'Ubah Sosial Media';
    $data['account'] = Account::where('accounts.uid', $id)->first();
    $data['users'] = User::all()->whereNotIn('role_id', 1);
    return view('accounts.editSosmed', $data);
  }

  public function updateAccount(Request $request){
    try {
      $data = $request->all();
      $data['query'] = Account::where('uid', $data['account_id']);
      $data['query']->update([ 'name' => $data['name'] ]);

      $_twt = $data['query']->first()->account_twt;
      $_ig  = $data['query']->first()->account_ig;
      $_fb  = $data['query']->first()->account_fb;
      $_yt  = $data['query']->first()->account_yt;

      try {
        if(isset($data['tw'])){
          $twt_ =  Twitter::getUsers(['screen_name' => $data['tw']]);
          if($_twt){
            $_twt->update([ 
              'account' =>  $data['tw'] ,
              'post' => $twt_->statuses_count,
              'followers' => $twt_->followers_count,
              'following' => $twt_->friends_count,
            ]);
          }else{
            Account_twt::create([
              'user_account' => $data['account_id'],
              'account' =>  $data['tw'] ,
              'post' => $twt_->statuses_count,
              'followers' => $twt_->followers_count,
              'following' => $twt_->friends_count,
            ]);
          }
        }
      } catch (\Throwable $th) {
        // return redirect('/account/edit/'.$data['account_id'])->with('error', 'Twitter anda salah!');
      }

      try {
        if(isset($data['ig'])){
          $cachePool = new FilesystemAdapter('Instagrams', 0, __DIR__ . '/../cache');
          $api = new Api($cachePool);
          $api->login('dirgaputraa', 'vanmarwijk1892');
          $ig_ = $api->getProfile($data['ig']);
          if($_ig){
            $_ig->update([ 
              'account' =>  $data['ig'] ,
              'post' => $ig_->getMediaCount(),
              'followers' => $ig_->getFollowers(),
              'following' => $ig_->getFollowing(),
            ]);
          }else{
            Account_ig::create([
              'user_account' => $data['account_id'],
              'account' => $data['ig'],
              'post' => $ig_->getMediaCount(),
              'followers' => $ig_->getFollowers(),
              'following' => $ig_->getFollowing(),
            ]);
          }
        }
      } catch (\Throwable $th) {
        // return redirect('/account/edit/'.$data['account_id'])->with('error', 'Instagram anda salah!');
      }

      try {
        if(isset($data['fb'])){
          if($_fb){
            $_fb->update([ 
              'account' =>  $data['fb'],
              'post' => 0,
              'followers' => 0,
              'following' => 0,
            ]);
          }else{
            Account_fb::create([
              'user_account' => $data['account_id'],
              'account' => $data['fb'],
              'post' => 0,
              'followers' => 0,
              'following' => 0,
            ]);
          }
        }
      } catch (\Throwable $th) {
        // return redirect('/account/edit/'.$data['account_id'])->with('error', 'Facebook anda salah!');
      }

      try {
        if(isset($data['yt'])){
          $yt_ = Youtube::getChannelById($data['yt']);
          if($_yt){
            $_yt->update([ 
              'account' =>  $data['yt'] ,
              'post' => $yt_->statistics->videoCount,
              'followers' => $yt_->statistics->subscriberCount,
              'following' => $yt_->statistics->subscriberCount,
            ]);
          }else{
            Account_yt::create([
              'user_account' => $data['account_id'],
              'account' => $data['yt'],
              'post' => $yt_->statistics->videoCount,
              'followers' => $yt_->statistics->subscriberCount,
              'following' => $yt_->statistics->subscriberCount,
            ]);
          }
        }
      } catch (\Throwable $th) {
        // return redirect('/account/edit/'.$data['account_id'])->with('error', 'Youtube anda salah!');
      }
      
      return redirect($data['url'])->with('status', 'Data '.$data['name'].' berhasil berubah!');
    } catch (\Throwable $th) {
      return redirect($data['url'])->with('error', 'Data Gagal Berubah!');
      // return redirect($data['url'])->with('error', array(
      //   'twt' => 'Twitter anda salah',
      //   'ig' => 'Twitter anda salah',
      //   'fb' => 'Twitter anda salah',
      //   'fb' => 'Twitter anda salah',
      // ));
    }
  }

}
