<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Activity_master;
use App\Activity;
use App\User;
use App\Exports\ActivityExport;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromView;
use Redirect;

class ActivityController extends Controller{
   public function __construct(){ 
      $this->middleware('auth');
   }
   public function getParams($param){
    $_slug = array( 'kampung-tertib-lalu-lintas', 'smart-city', 'taman-lantas', 'pks', 'cara-aman-ke-sekolah', 'masdarwis', 'pocil', 'kampus-pelopor-tertib-berlalu-lintas', 'pembinaan-komunitas', 'pameran', 'manajemen-media', '7-program-prioritas', 'indonesia-layak-anak-idola' );
    if(in_array($param, $_slug)){
      return true;
    }else{
      return false;
    }
   }
   
   public function index(Request $request, $slug){
    $checkParams = $this->getParams($slug);
    if($checkParams){
      $getId = Activity_master::where('activity_master.slug', $slug)
      ->first();
      $data['title'] = 'Aktifitas';
      $data['query'] = Activity::orderBy('created_at', 'desc')
      // ->where(function($q) use ($request){
      //   $q->where('activity_id', $getId->id)

      // })

      ->where('activity_id', $getId->id)
      ->whereHas('user', function($query) use($request) {
        if(Auth::user()->role_id == 3){
          $query->where('polda_id', Auth::user()->polda_id);
        }else if(Auth::user()->role_id == 4){
          $query->where('polres_id', Auth::user()->polres_id);
        }
        
        $query->where(function($q) use($request){
          $q->orWhere('name', 'like', "%{$request->search}%")->orWhereHas('role', function($qq) use($request){
            $qq->where('role_name', 'like', "%{$request->search}%");
          });
        });
      });
      $data['total'] = $data['query']->count();
      $data['activity'] = $data['query']->paginate(10);
      $data['activity']->appends($request->only('search'));
      return view('activity.activity', $data);
    }else{
      return redirect('/activity/all');
    }
   }

  public function export(Request $request, $slug){
    $ttl = 'Report-'.str_replace('-', ' ', $slug).'.xlsx';
    return Excel::download(new ActivityExport($slug), $ttl);
  }
    
  public function all(Request $request){
    $data['title'] = 'Semua Aktifitas';
    $data['query'] = Activity::orderBy('created_at', 'desc')
    ->whereHas('user', function($query) use($request) {
      if(Auth::user()->role_id == 3){
        $query->where('polda_id', Auth::user()->polda_id);
      }else if(Auth::user()->role_id == 4){
        $query->where('polres_id', Auth::user()->polres_id);
      }
      $query->where(function($q) use($request){
        $q->orWhere('name', 'like', "%{$request->search}%")->orWhereHas('role', function($qq) use($request){
          $qq->where('role_name', 'like', "%{$request->search}%");
        });
      });
    });
    $data['total'] = $data['query']->count();
    $data['activity'] = $data['query']->paginate(10);
    $data['activity']->appends($request->only('search'));
    return view('activity.activity', $data);
  }
    
  public function detail(Request $request, $id){
    $data['title'] = 'Detil Aktifitas';
    $data['_activity'] = Activity::where('uid', $id)->first();
    return view('activity.detailActivity', $data);
  }
   
  //  public function alls(Request $request){
  //   $data['title'] = 'Semua Aktifitas';
  //   $data['query'] = Activity::orderBy('created_at', 'desc')
  //   ->whereHas('user', function($query) use($request){
  //     $query->where('name', 'like', "%{$request->search}%")
  //       ->orWhereHas('role', function($query) use($request){
  //         $query->where('role_name', 'like', "%{$request->search}%");
  //       });
  //   })
  //   ->orWhereHas('activity_master', function($query) use($request){
  //     $query->where('slug', 'like', "%{$request->search}%");
  //   })
  //   ->orWhere('peserta', 'like', "%{$request->search}%");
  //   $data['total'] = $data['query']->count();
  //   $data['activity'] = $data['query']->paginate(10);
  //   $data['activity']->appends($request->only('search'));
  //   return view('activity.activity', $data);
  //  }

   public function create(){
    $data['title'] = 'Tambah Aktifitas';
    $data['users'] = User::orderBy('created_at', 'desc')->get();
    $data['activity_master'] = Activity_master::orderBy('id', 'asc')->get();
    return view('activity.createActivity', $data);
   }

   public function store(Request $request){
    try {
      $data = $request->all();
      $auth = Auth::user();
      $request->validate([
        'activity_id' => 'required',
        'target' => 'required',
        'pelaksanaan' => 'required',
        'peserta' => 'required',
        // 'dokumentasi' => $data['type'] === 'link' ? 'required' : 'required|mimes: jpg,png,jpeg,pdf',
        'dokumentasi' => $data['type'] === 'link' ? 'required' : 'required',
      ]);
      if($data['type'] === 'file'){
        $fileName = time().'_'.$request->dokumentasi->getClientOriginalName();
        $filePath = $request->file('dokumentasi')->move('uploads/activity', $fileName);
      }
      Activity::create([
        'uid' =>  Str::uuid(),
        'user_id' =>  $auth->role_id === 1 ? $data['user_id'] : $auth->uuid,
        "activity_id" => $data['activity_id'],
        "target" => date("Y-m-d H:m:s", strtotime($data['target'])),
        "pelaksanaan" => date("Y-m-d H:m:s", strtotime($data['pelaksanaan'])),
        "peserta" => $data['peserta'],
        "dokumentasi" => $data['type'] === 'link' ? $data['dokumentasi'] : $fileName,
        "type" => $data['type'],
        "path" => $data['type'] === 'link' ? NULL : $filePath,
        "created_at" => date('Y-m-d H:i:s'),
      ]);
      return redirect('/activity/create')->with('status', 'Data berhasil di masukan!');
    } catch (\Throwable $th) {
      // print_r($th->getMessage());
      return redirect('/activity/create')->with('error', $th->getMessage());
    }
   }
   
   public function edit(Request $request, $id){
    $data['title'] = 'Ubah Aktifitas';
    $data['users'] = User::orderBy('created_at', 'desc')->get();
    $data['activity_master'] = Activity_master::orderBy('id', 'asc')->get();
    $data['activity'] = Activity::where('uid', $id)->first();
    return view('activity.editActivity', $data);
   }

   public function editStore(Request $request, $id){
    try {
      $data = $request->all();
      $query = Activity::where('uid', $id);
      $auth = Auth::user();
      if($data['type'] === 'file'){
        $fileName = time().'_'.$request->dokumentasi->getClientOriginalName();
        $filePath = $request->file('dokumentasi')->move('uploads/activity', $fileName);
        if(File::exists($query->first()->path)) {
          File::delete($query->first()->path);
        }
      }
      $query->update([
        'user_id' =>  $auth->role_id === 1 ? $data['user_id'] : $auth->uuid,
        "activity_id" => $data['activity_id'],
        "target" => date("Y-m-d H:m:s", strtotime($data['target'])),
        "pelaksanaan" => date("Y-m-d H:m:s", strtotime($data['pelaksanaan'])),
        "peserta" => $data['peserta'],
        "dokumentasi" => $data['type'] === 'link' ? $data['dokumentasi'] : $fileName,
        "path" => $data['type'] === 'link' ? NULL : $filePath,
        "type" => $data['type'],
      ]);
      return redirect('/activity/edit/'.$id)->with('status', 'Data berhasil di masukan!');
    } catch (\Throwable $th) {
      // dd($request->dokumentasi);
      // dd($th);
      return redirect('/activity/edit/'.$id)->with('error', 'Data gagal di masukan!');
    }
   }

  public function delete(Request $request){ 
    $data = $request->all();
    $query = Activity::where('uid', $data['id']);
    if(File::exists($query->first()->path)) {
      File::delete($query->first()->path);
    }
    $query->delete();
    $status = array(
      "status" => 200,
      "id" => $data['id'],
    );
    return $status;
  }

}
