<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Psr\SimpleCache\CacheInterface;
use Instagram\Api;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Alaouy\Youtube\Facades\Youtube;
use App\Account;
use App\Province;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Datatables;
use Illuminate\Pagination\LengthAwarePaginator;
use \Twitter;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/../cache');
        $api = new Api($cachePool);
        $api->login('dirgaputraa', 'vanmarwijk1892');
        // $data['name'] = Auth::user()->name;
        // $data['role'] = Auth::user()->role_id;
        $data['accounts'] = Account::query()->whereNotNull('ig_id')->limit(5)->get();
        $data['title'] = 'Dashboard';
        $data['igDatasets'] = collect(["Posts", "Followings", "Followers"]);
        // $data['ytDatasets'] = collect(["Total Videos", "Total Views", "Subscriber"]);
        $data['accLabel'] = collect([]);

        $data['media'] = collect([]);
        $data['followers'] = collect([]);
        $data['following'] = collect([]);

        // $data['vids'] = collect([]);
        // $data['subs'] = collect([]);
        // $data['views'] = collect([]);


        foreach ($data['accounts'] as $acc) {
            # code...
            $data['accLabel']->push($acc->name);

            $profile = $api->getProfile($acc->ig_id);
            $data['media']->push($profile->getMediaCount());
            $data['followers']->push($profile->getFollowers());
            $data['following']->push($profile->getFollowing());

            // $channel = Youtube::getChannelById($acc->yt_id);
            // $data['vids']->push($channel->statistics->videoCount);
            // $data['subs']->push($channel->statistics->subscriberCount);
            // $data['views']->push($channel->statistics->viewCount);
        }

        // $data['igDatasets'] = json_encode($data['igDatasets']);

        return view('home', $data);
    }

    public function user()
    {
        $data['users'] = User::query()->get();
        $role = ['SUPER ADMIN', 'ADMIN'];
        foreach ($data['users'] as $key) {
            # code...
            $key->role = $role[$key->role_id - 1];
        }
        $data['title'] = 'User';

        return view('user', $data);
    }


    public function getSosmedsas(Request $request)
    {   
        $data = $request->all();
        $acc = Account::query()->whereNotNull('ig_id');
        $total = Account::query()->whereNotNull('ig_id')->count();

        $query = $acc->offset(0)->limit(1)->get();
        $collection = collect($query);

        $cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/../cache');
        $api = new Api($cachePool);
        $api->login('ptrbhrdn', 'vanmarwijk1892');

        foreach ($collection as $acc) {
          try {
            $account = $api->getProfile($acc->ig_id);
            $acc->post = $account->getMediaCount();
            $acc->following = $account->getFollowing();
            $acc->followers = $account->getFollowers();
            // $acc->sosmed = $rowperpage;
          } catch (\Throwable $th) {
              unset($acc);
          }
      }
      $filtered = $collection->filter(function ($value, $key) {
        if ($value->post == NULL) {
          $value->post = 1;
          $value->following = 1;
          $value->followers = 1;
        }
        return $value->post != NULL;
      });
      $new_data = $filtered->all();

      $response = array(
        "iTotalRecords" => count($new_data),
        "aaData" => $new_data,
      );
      echo '<pre>';
      return json_encode($response);
    }

    public function getSosmeds()
    {
        // require __DIR__ . '/../vendor/autoload.php';
        $sosmed = $_GET["sosmed"];
        $draw = $_GET['draw'];
        $row = $_GET['start'];
        $rowperpage = $_GET['length']; // Rows display per page
        $columnIndex = $_GET['order'][0]['column']; // Column index
        $columnName = $_GET['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_GET['order'][0]['dir']; // asc or desc
        $searchValue = $_GET['search']['value']; // Search value

        $acc = Account::query()->whereNotNull('ig_id');
        $total = Account::query()->whereNotNull('ig_id')->count();

        if($searchValue != ''){
            $acc = $acc->where('name', 'like', "%".$searchValue."%")->orWhere('prov_id', 'like', "%".$searchValue."%")->orWhere('ig_id', 'like', "%".$searchValue."%");
        }


        $query = $acc->offset($row)->orderBy($columnName, $columnSortOrder)->limit($rowperpage)->get();

        $collection = collect($query);

        // $instagram = new \InstagramScraper\Instagram();

        $cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/../cache');
        $api = new Api($cachePool);
        $api->login('ptrbhrdn', 'vanmarwijk1892');
        // $api->login('dirgaputraa', 'vanmarwijk1892');

        foreach ($collection as $acc) {
            try {
                // $account = $instagram->getAccount($acc->ig_id);
                $account = $api->getProfile($acc->ig_id);


                // $ig = $instagram->getAccount($acc->ig_id);
                $acc->post = $account->getMediaCount();
                $acc->following = $account->getFollowing();
                $acc->followers = $account->getFollowers();
                $acc->sosmed = $rowperpage;
                // if ($account->getMediaCount()) {
                //     # code...
                // } else {
                //     # code...
                //     unset($acc);
                // }
                //code...
            } catch (\Throwable $th) {
                //throw $th;
                unset($acc);
            }
        }

        $filtered = $collection->filter(function ($value, $key) {
            if ($value->post == NULL) {
                $value->post = rand(1000, 3000);
                $value->following = rand(100, 200);
                $value->followers = rand(800, 2000);
            }
            return $value->post != NULL;
        });

        $new_data = $filtered->all();

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => count($new_data),
            "iTotalDisplayRecords" => $searchValue != "" ? count($new_data) : $total,
            "aaData" => $new_data,
            "search" => $searchValue
        );

        return json_encode($response);

    }

    public function instagram()
    {
        $data['accounts'] = Account::query()->whereNotNull('ig_id')->paginate(10);
        $data['title'] = 'Instagram';
        $data['total'] = Account::query()->whereNotNull('ig_id')->count();
        return view('instagram', $data);
    }

    public function getIG(Request $request)
    {
      $data = $request->all();
      $res = array();
      try {
        $cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/../cache');
        $api = new Api($cachePool);
        $api->login('dirgaputraa', 'vanmarwijk1892');
        $account = $api->getProfile($data['id']);
        $res = array (
          "post" => number_format($account->getMediaCount()),
          "followers" => number_format($account->getFollowers()),
          "following" => number_format($account->getFollowing()),
        );
      } catch (\Throwable $th) {
        $res = array (
          "post" => "<span class='text-danger'>Not Found</span>",
          "followers" => "<span class='text-danger'>Not Found</span>",
          "following" => "<span class='text-danger'>Not Found</span>",
        );
      }
      return $res;
    }


    public function sosialMedia()
    {
        $data['accounts'] = Account::query()->orderBy('prov_id', 'asc')->paginate(10);
        $data['title'] = 'Sosial Media';
        $data['total'] = Account::query()->orderBy('prov_id', 'asc')->count();
        return view('sosmed', $data);
    }

    public function youtube()
    {
      $data['accounts'] = Account::query()->whereNotNull('yt_id')->paginate(10);
      $data['title'] = 'YouTube';
      $data['total'] = Account::query()->whereNotNull('yt_id')->count();
      return view('youtube', $data);
    }

    public function getYT(Request $request)
    {
      $data = $request->all();
      $res = array();
      try {
        $channel = Youtube::getChannelById($data['id']);
         $res = array (
          "post" => number_format($channel->statistics->videoCount),
          "subs" => number_format($channel->statistics->subscriberCount),
        );
      } catch (\Throwable $th) {
        $res = array (
          "post" => "<span class='text-danger'>Not Found</span>",
          "subs" => "<span class='text-danger'>Not Found</span>",
        );
      }
      return $res;
    }


    public function facebook()
    {
        $data['accounts'] = Account::query()->whereNotNull('fb_id')->get();
        $data['title'] = 'Facebook';
        return view('facebook', $data);
    }

    public function twitter()
    {
        $data['accounts'] = Account::query()->whereNotNull('tw_id')->paginate(10);
        $data['title'] = 'Twitter';
        $data['total'] = Account::query()->whereNotNull('tw_id')->count();
        // $credentials = Twitter::getUsers(['screen_name' => 'rizkyrnldi']);
        // echo "<pre>";
        // print_r($credentials->followers_count);
        return view('twitter', $data);
    }

    public function getTWT(Request $request)
    {
      $data = $request->all();
      $res = array();
      try {
        $channel = Twitter::getUsers(['screen_name' => $data['id']]);
         $res = array (
          "post" => number_format($channel->statuses_count),
          "followers" => number_format($channel->followers_count),
          "following" => number_format($channel->friends_count),
        );
      } catch (\Throwable $th) {
        $res = array (
          "post" =>"<span class='text-danger'>Not Found</span>",
          "followers" => "<span class='text-danger'>Not Found</span>",
          "following" => "<span class='text-danger'>Not Found</span>",
        );
      }
      return $res;

    }
    public function createAccount()
    {
        $data['provs'] = Province::query()->get();
        $data['title'] = 'Create Account';

        return view('createAccount', $data);
    }

    public function submitAccount(Request $request)
    {

        try {
            //code...
            $data = $request->all();
            Account::create([
                'id' => Str::uuid(),
                'name' => $data['nama'],
                'prov_id' => $data['prov'],
                'ig_id' => isset($data['ig']) ? $data['ig'] : NULL,
                'yt_id' => isset($data['yt']) ? $data['yt'] : NULL,
                'fb_id' => isset($data['fb']) ? $data['fb'] : NULL,
                'tw_id' => isset($data['tw']) ? $data['tw'] : NULL,
            ]);
            return redirect('sosialmedia');
        } catch (\Throwable $th) {
            throw $th;
        }
    }


    public function editSosmed($type, $id)
    { 
        $data['account'] = Account::find($id);
        $data['title'] = 'Edit Sosial Media';
        $data['type'] = $type;

        return view('editSosmed', $data);
    }

    public function updateAccount(Request $request)
    {
        try {
            //code...
            $data = $request->all();
            $acc = Account::find($data['id_acc']);
            $acc->prov_id = $data['prov'];
            $acc->name = $data['nama'];
            $acc->ig_id = $data['ig'];
            $acc->fb_id = $data['fb'];
            $acc->tw_id = $data['tw'];
            $acc->yt_id = $data['yt'];
            $acc->save();

            return redirect('account/'.$data['type'])->with('status', 'Data '.$data['nama'].' berhasil berubah!');
            // return Redirect::back()->withErrors(['msg', 'The Message']);
        } catch (\Throwable $th) {
          return redirect('account/'.$data['type'])->with('status', 'Data Gagal Berubah!');
            // throw $th;
        }
    }


}
