<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Redirect;
use App\Province;
use App\User;
use App\Role;
use App\Polri;
use App\Polda;
use App\Polres;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class UserController extends Controller{
   
  public function __construct(){ 
    $this->middleware('auth');
  }

  public function index(Request $request){
    $data['title'] = 'User';
    $data['query'] = User::with(['polri'])
    ->where(function($q) use ($request){
      if(Auth::user()->role_id == 3){
        $q->where('polda_id', Auth::user()->polda_id);
      }else if(Auth::user()->role_id == 4){
        $q->where('polres_id', Auth::user()->polres_id);
      }
    })
    ->where(function($q) use ($request){
      if(Auth::user()->role_id === 1 || Auth::user()->role_id === 2){
        $q->orWhereHas('polri', function($query) use($request){
          $query->where('name_polri', 'like', "%{$request->search}%");
        })->orWhereHas('polda', function($query) use($request){
          $query->where('name_polda', 'like', "%{$request->search}%");
        })->orWhereHas('polres', function($query) use($request){
          $query->where('name_polres', 'like', "%{$request->search}%");
        });
      }else if(Auth::user()->role_id === 3){
        $q->orWhereHas('polda', function($query) use($request){
          $query->where('name_polda', 'like', "%{$request->search}%");
        })->orWhereHas('polres', function($query) use($request){
          $query->where('name_polres', 'like', "%{$request->search}%");
        });
      }else if(Auth::user()->role_id === 4){
        $q->whereHas('polres', function($query) use($request){
          $query->where('name_polres', 'like', "%{$request->search}%");
        });
      }
      $q->orWhere('name', 'like',  '%'.$request->search.'%')->orWhere('email', 'like',  '%'.$request->search.'%');
    })
    ->orderBy('created_at', 'desc');

    $data['total'] = $data['query']->count();
    $data['users'] = $data['query']->paginate(10);
    $data['users']->appends($request->only('search'));
    return view('users.user', $data);
  }
  
  public function create(){
    $data['title'] = 'Buat User';
    $data['role'] = Role::select()->whereNotIn('id', [1])->get();
    $data['polri'] = DB::table('polri')->get();
    return view('users.createUser', $data);
  }
  
  public function submit(Request $request){
    $data = $request->all();
    try {
      if ($data['role'] === "2") {
        $subRole_id = $data['polri'];
      }else if($data['role'] === "3"){
        $subRole_id = $data['polda'];
      }else{
        $subRole_id = $data['polres'];
      };

      if($data['password'] === $data['cPassword']){
        // dd($data);
        User::create([
          'uuid' => Str::uuid(),
          'name' => $data['name'],
          'email' => $data['email'],
          'password' => Hash::make($data['password']),
          'role_id' => $data['role'],
          'polri_id' => $data['polri'],
          'polda_id' => isset($data['polda']) ? $data['polda'] : 0,
          'polres_id' => isset($data['polres']) ? $data['polres'] : 0,
        ]);
        return redirect('/user')->with('status', 'Data '.$data['name'].' berhasil di input!');
      }else{
        return redirect('/user/create')->with('error', 'Password dan confirm password tidak sama');
      }
    } catch (\Throwable $th) {
      // dd($th->errorInfo[2]);
      // return redirect('/user/create')->with('error', 'Data gagal di input!');
      return redirect('/user/create')->with('error', $th->errorInfo[2]);
    }
  }

  public function edit($id){
    try {
      $data['title'] = 'Ubah User';
      $data['polri'] = Polri::get();
      $data['polda'] = Polda::get();
      $data['polres'] = Polres::get();
      $data['role'] = Role::select()->whereNotIn('id', [1])->get();
      $data['users'] = User::where('uuid', $id)->first();
      return view('users.editUser', $data);
    }catch (\Throwable $th) {
      return redirect('/user')->with('error', 'Data tidak di temukan!');
    }
  }
  
  public function storeEdit(Request $request){
    $data = $request->all();
    try {
      DB::table('users')->where('id', $data['id'])->update([
        'name' => $data['name'],
        'email' => $data['email'],
        'role_id' => $data['role_id'],
      ]);
      return redirect('/user')->with('status', 'Data '.$data['name'].' berhasil di ubah!');
    }catch (\Throwable $th) {
      return redirect('/user')->with('error', 'Data '.$data['name'].' gagal di ubah!');
    }
  }

  public function password($id){
    try {
      $data['title'] = 'Ubah User Password';
      $data['users'] = User::where('uuid', $id)->first();
      return view('users.editPassword', $data);
    }catch (\Throwable $th) {
      return redirect('/user')->with('error', 'Data tidak ditemukan!');
    }
  }
  
  public function storePassword(Request $request, $id){
    $data = $request->all();
    try {
      if($data['password'] === $data['cPassword']){
        DB::table('users')->where('uuid', $id)->update([
          'password' => Hash::make($data['password'])
        ]);
        return redirect('/user')->with('status', 'Password berhasil diubah!');
      }else{
        return redirect('/user/password/'.$id)->with('error', 'Password dan Confirm Password Tidak Sama');
      }
    }catch (\Throwable $th) {
      return redirect('/user/password/'.$id)->with('error', 'Password gagal diubah!');
    }
  }

  public function delete(Request $request){
    $data = $request->all();
    try {
      DB::table('users')->where('id', $data['id'])->delete();
      $status = array(
        "status" => 200,
        "id" => $data['id'],
        "data" => '$query->first()',
      );
      return $status;
    }catch (\Throwable $th) {
      $status = array(
        "status" => 400,
        "data" => '$query->first()',
      );
      return $status;
    }
  }
  

}
