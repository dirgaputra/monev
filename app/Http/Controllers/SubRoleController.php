<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubRoleController extends Controller{
  
  public function getPolda(Request $request){
    $data = $request->all();
    $query = DB::table('polda')->where('id_polri', $data['id'])->get();
    $status = array(
      'data' => $query,
    );
    return $status;
  }
  
  public function getPolres(Request $request){
    $data = $request->all();
    $query = DB::table('polres')->where('id_polda', $data['id'])->get();
    $status = array(
      'data' => $query,
    );
    return $status;
  }

}
