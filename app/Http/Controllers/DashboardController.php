<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Redirect;
use Psr\SimpleCache\CacheInterface;
use Instagram\Api;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Alaouy\Youtube\Facades\Youtube;
use App\Account;
use App\Province;
use App\Video;
use App\User;
use App\Activity;
use App\Activity_master;
use App\Polda;
use App\Slider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller{
   
  public function __construct(){ 
    $this->middleware('auth');
  }

  public function indexs(Request $request){
    $query = user::with(['Activity' => function($query){
      $query->select('uid', 'user_id', 'activity_id');
    }])->get()->toArray();
    $label = [];
    $polda = [];
    $activity = [];
    $qq = [];

    echo '<pre>';
   
    foreach (Polda::all()->toArray() as $key => $value) {
      $label[] = $value['name_polda'];
      $polda[$value['id']] = [];
    }

    foreach (Activity_master::all()->toArray() as $key => $value) {
      $activity[$value['id']] = $polda;
    }

    foreach ($query as $key => $value) {
      if($value['polda_id'] > 0){
        foreach ($value['activity'] as $tt => $ac) {
          $activity[$ac['activity_id']] [$value['polda_id']][] = $ac['activity_id'];
        }
      }
    }

    $final = [];
    foreach ($activity as $key => $qq) {
      $final[$key] = $qq;
      foreach ($qq as $_key => $qqs) {
        if (count($qqs) > 0) {
          $final[$key][$_key] = count($qqs);
        }else{
          $final[$key][$_key] = 0;
        }
      }
    }
    // print_r($activity);
    // print_r($polda);
    // print_r($query);
    print_r($final);
  }


  public function index(Request $request){
    $data['title'] = 'Dashboard';
    $data['video'] = Video::orderBy('created_at', 'desc')->first();
    $data['slider'] = Slider::where('type', 'infografis')->orderBy('created_at', 'desc')->get();
    $query = Account::all();    
    $data['label'] = collect([]);

    $data['data_ig'] = collect([]);

    $data['data_post_ig'] = collect([]);
    $data['data_color_ig'] = collect([]);
    $data['data_followers_ig'] = collect([]);
    $data['data_following_ig'] = collect([]);

    $data['data_post_twt'] = collect([]);
    $data['data_followers_twt'] = collect([]);
    $data['data_following_twt'] = collect([]);

    $data['data_post_fb'] = collect([]);
    $data['data_followers_fb'] = collect([]);
    $data['data_following_fb'] = collect([]);

    $data['data_post_yt'] = collect([]);
    $data['data_followers_yt'] = collect([]);
    $data['data_following_yt'] = collect([]);
    
    $data['totalAcc'] = 0;

    $query = Account::where('name', 'like', "%{$request->search}%")
    ->get();
    $nquery = $request->orderBy === 'asc' ? $query->sortBy($request->sosmed.'.post') : $query->sortByDesc($request->sosmed.'.post') ;

    $i = 1;
    foreach ($nquery as $key) {
      $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
      $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
      $data['totalAcc'] = count($query);
      if($key->account_ig){
        $data['label']->push($key->name);
        $data['data_color_ig']->push($color);
        $data['data_post_ig']->push($key->account_ig->post);
        $data['data_followers_ig']->push($key->account_ig->followers);
        $data['data_following_ig']->push($key->account_ig->following);
      }

      if($key->account_twt){
        $data['data_post_twt']->push($key->account_twt->post);
        $data['data_followers_twt']->push($key->account_twt->followers);
        $data['data_following_twt']->push($key->account_twt->following);
      }
      
      if($key->account_yt){
        $data['data_post_yt']->push($key->account_yt->post);
        $data['data_followers_yt']->push($key->account_yt->followers);
        $data['data_following_yt']->push($key->account_yt->following);
      }

      if($key->account_fb){
        $data['data_post_fb']->push($key->account_fb->post);
        $data['data_followers_fb']->push($key->account_fb->followers);
        $data['data_following_fb']->push($key->account_fb->following);
      }
    };
    

    $data['countIG'] =  DB::table('account_igs')->count();
    $data['countFB'] =  DB::table('account_fbs')->count();
    $data['countTWT'] =  DB::table('account_twts')->count();
    $data['countYT'] =  DB::table('account_yts')->count();


    // activity chart
    $query = user::with(['Activity' => function($query){
      $query->select('uid', 'user_id', 'activity_id');
    }])->get();
    $data['labelAct'] = [];
    $polda = [];
    $activity = [];

    // echo '<pre>';
   
    foreach (Polda::all()->toArray() as $key => $value) {
      $data['labelAct'][] = $value['name_polda'];
      $polda[$value['id']] = [];
    }

    foreach (Activity_master::all()->toArray() as $key => $value) {
      $activity[$value['id']] = $polda;
    }

    foreach ($query as $key => $value) {
      if($value['polda_id'] > 0){
        foreach ($value['activity'] as $tt => $ac) {
          $activity[$ac['activity_id']] [$value['polda_id']][] = $ac['activity_id'];
        }
      }
    }
    
    $data['activity'] = [];
    foreach ($activity as $key => $qq) {
      $data['activity'][$key] = $qq;
      foreach ($qq as $_key => $qqs) {
        if (count($qqs) > 0) {
          $data['activity'][$key][$_key] = count($qqs);
        }else{
          $data['activity'][$key][$_key] = 0;
        }
      }
    }

    return view('dashboards.dashboard', $data);

  }

  public function render(Request $request){
  }

}
