<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Slider;
use Redirect;

class SliderController extends Controller{
  public function __construct(){ 
    $this->middleware('auth');
  }

  public function index(Request $request){
    $data['title'] = 'Slider';
    $data['query'] = Slider::orderBy('created_at', 'desc');
    $data['total'] = $data['query']->count();
    $data['slider'] = $data['query']->paginate(10);
    $data['slider']->appends($request->only('search'));
    return view('slider.slider', $data);
  }
  
  public function create(){
    $data['title'] = 'Tambah Slider';
    return view('slider.createSlider', $data);
  }
  
  public function store(Request $request){
    try {
      $auth = Auth::user();
      $request->validate([
        'slider' => 'required',
        // 'slider' => 'required|mimes: jpg,png,jpeg',
      ]);
      $fileName = time().'_'.$request->slider->getClientOriginalName();
      $filePath = $request->file('slider')->move('uploads/sliders', $fileName);
      Slider::create([
        'uid' =>  Str::uuid(),
        "author" => $auth->uuid,
        "fileName" => $fileName,
        "path" => $filePath,
        "type" => $request->type,
        "created_at" => date('Y-m-d H:i:s'),
      ]);
      return redirect('/slider')->with('status', 'Data berhasil di masukan!');
    } catch (\Throwable $th) {
      return redirect('/slider')->with('error', 'Data gagal di masukan!');
    }
  }

  public function edit(Request $request, $id){
    $data['title'] = 'Ubah Slider';
    $data['slider'] = Slider::where('uid', $id)->first();
    if($data['slider']){
      return view('slider.editSlider', $data);
    }else{
      return redirect('/slider');
    }
  }

   public function editStore(Request $request, $id){
    $auth = Auth::user();
    $query = Slider::where('uid', $id);
    if($request->slider){
      $fileName = time().'_'.$request->slider->getClientOriginalName();
      $filePath = $request->file('slider')->move('uploads/sliders', $fileName);
      if(File::exists($query->first()->path)) {
        File::delete($query->first()->path);
      }
    }
    $query->update([
      "fileName" => $request->slider ? $fileName : $query->first()->fileName,
      "path" => $request->slider ? $filePath : $query->first()->path,
      "type" => $request->type,
    ]);
    return redirect('/slider')->with('status', 'Data berhasil di masukan!'); 
  
   }

   public function delete(Request $request){ 
    $data = $request->all();
    $query = Slider::where('uid', $data['id']);
    if(File::exists($query->first()->path)) {
      File::delete($query->first()->path);
    }
    $query->delete();
    $status = array(
      "status" => 200,
      "id" => $data['id'],
    );
    return $status;
  }


}
