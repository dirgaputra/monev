<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable = [
        'uid', 'user_id', 'activity_id', 'target', 'pelaksanaan', 'peserta', 'dokumentasi', 'type', 'path', 'created_at'
    ];

    public function activity_master(){
    	return $this->hasOne(Activity_master::class, 'id', 'activity_id');
    }

    public function user(){
    	return $this->hasOne(User::class, 'uuid', 'user_id');
    }

    protected $table = 'activity';
}
