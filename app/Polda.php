<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Polda extends Model
{
    public function user(){
        // return $this->hasMany(User::class, 'polda_id', 'id');
        return $this->hasMany(User::class, 'polda_id',  'id');
    }
    public function activity(){
        return $this->hasManyThrough(Activity::class, User::class, 'uuid', 'user_id', 'id', 'polda_id');
        // return $this->hasOne(Polda::class, 'id', 'polda_id');
    }
    
    protected $table = 'polda';
}
