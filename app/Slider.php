<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    use Notifiable;
    protected $fillable = [
        'uid', 'fileName', 'path', 'author', 'type', 'created_at'
    ];

    public function user(){
    	return $this->hasOne(User::class, 'uuid', 'author');
    }
}
